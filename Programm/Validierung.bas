Attribute VB_Name = "Validierung"
Option Explicit

'Definition Constanten f�r W�rmekapazit�tn und m1
Const cp1 As Double = 4190    'J/kg/K
Const cp2 As Double = 1100    'J/kg/K
Const m1 As Double = 1000     'kg/h

Public Sub Validierung_Nachrechnung_Gleichstrom()
    Call Validierung_Nachrechnung(Gleichstrom)
End Sub

Public Sub Validierung_Nachrechnung_Gegenstrom()
    Call Validierung_Nachrechnung(Gegenstrom)
End Sub

Public Sub Validierung_Nachrechnung(typ As Bauform)
    
    'Definition der Variablen
    Dim ws As Worksheet     'aktives Arbeitsblatt
    Dim A                   '�bertragerfl�che in m�
    Dim k                   'Kapazit�tsstromverh�ltnis
    Dim m2 As Double        'Massenstrom 2 in kg/h
    Dim t1ein               'Eintrittstemperatur von Stoffstrom 1 in �C
    Dim t2ein As Double     'Eintrittstemperatur von Stoffstrom 2 in �C
    Dim dt As Integer       'Temperaturunterscheid am Eintritt in K
    Dim row As Integer      'Reihe
    Dim column As Integer   'Spalte
    Dim x As Integer        'Hilfsvariable
    Dim y As Integer        'Hilfsvariable
    Dim diff As Double      'Abweichung der Berechnungsergebnisse
    Dim max As Double       'maximale Abweichung
    
    'Berechnungsergebnisse
    Dim t_At As Double      'Ausgleichstemperatur
    Dim t_Bc As Double      'Betriebscharakteristik
    
    'Arrays f�r Validierungsmatrx
    Dim t1(20) As Double      'Eintrittstemperaturen von Stoffstrom 1 in �C
    Dim cv(12) As Double    'Kapazit�tsstromverh�ltnisse
    Dim fl�che(11) As Double '�bertragerfl�chen in m�
    
    'Festlegen der Punkte der Validierungsmatrix
    '�bertragerfl�che
    fl�che(0) = 0.5
    fl�che(1) = 1
    fl�che(2) = 2
    fl�che(3) = 5
    fl�che(4) = 10
    fl�che(5) = 25
    fl�che(6) = 50
    fl�che(7) = 75
    fl�che(8) = 100
    fl�che(9) = 150
    fl�che(10) = 200
    fl�che(11) = 250
    
    'Kapazit�tsstromverh�ltnis
    cv(0) = 0.04
    cv(1) = 0.1
    cv(2) = 0.2
    cv(3) = 0.25
    cv(4) = 1 / 3
    cv(5) = 0.5
    cv(6) = 1
    cv(7) = 2
    cv(8) = 3
    cv(9) = 4
    cv(10) = 5
    cv(11) = 10
    cv(12) = 25
    
    'Eintrittstemperaturen Stoffstrom 1
    x = 0
    Do
        t1(x) = 5 * x
        x = x + 1
    Loop Until x = 21
    
    'Zwischenspeichern des aktiven Arbeitsblattes
    Set ws = ActiveSheet
    
    'Beschriftung des Tabellenblattes
    If typ = Gegenstrom Then
    ws.Cells(2, 2) = "Validierung Nachrechnung Gegenstrom"
    ElseIf typ = Gleichstrom Then
    ws.Cells(2, 2) = "Validierung Nachrechnung Gleichstrom"
    End If
    ws.Cells(3, 2) = "gr��te Abweichung"
    
    'Durchlaufen der Validierungsmatrix
    x = 0
    y = 0
    row = 14
    max = 0
    For Each A In fl�che()  'Durchlaufen �bertragerfl�chen
        column = 3
        For Each k In cv()  'Durchlaufen Kapazit�tsstromverh�ltnisse
            m2 = m1 * cp1 / k / cp2
            y = 0           'Laufvariable Spalten
            For Each t1ein In t1()  'Durchlaufen Eintrittstemperaturen Stoffstrom 1
                dt = 0.1
                x = 0               'Laufvaribale Reihen
                ws.Cells(row - 1, column - 1) = ChrW(916) + "t in K"  'Beschriftung
                ws.Cells(row - 2, column) = "t1ein in �C"
                If IsEmpty(ws.Cells(row - 1, column + y)) Then
                    ws.Cells(row - 1, column + y) = t1ein
                End If
                Do                  'Durchlaufen Eintrittstemperaturen Stoffstrom 2
                    t2ein = t1ein - dt
                    
                    'Berechnungs der Austrittstemepraturen von Stoffstrom 1
                    t_At = Ausgleichstemperatur_Nachrechnung(m1, cp1, t1ein, m2, cp2, t2ein, A, 50, typ)(0)
                    t_Bc = Betriebscharakteristik_Nachrechnung(m1, cp1, t1ein, m2, cp2, t2ein, A, 50, typ)(0)
                    diff = t_At - t_Bc
                    
                    ws.Cells(row + x, column + y) = diff
                    If Abs(diff) < 0.0000000001 Then
                        ws.Cells(row + x, column + y).Interior.Color = 13434828
                    Else
                        ws.Cells(row + x, column + y).Interior.Color = 7569657
                    End If
                    
                    If max < Abs(diff) Then
                        max = Abs(diff)
                    End If
                    If IsEmpty(ws.Cells(row + x, column - 1)) Then
                        ws.Cells(row + x, column - 1) = dt
                    End If
                    x = x + 1
                    If dt = 0.1 Then      'Erh�hung Temperaturunterschied
                        dt = 0.5
                    ElseIf dt = 0.5 Then
                        dt = 1
                    ElseIf dt = 1 Then
                        dt = 2
                    ElseIf dt = 2 Then
                        dt = 5
                    Else
                        dt = dt + 5
                    End If
                Loop Until t2ein = -30
                y = y + 1
            Next
            column = column + y + 2
        Next
        row = row + x + 4
    Next
    
    ws.Cells(3, 4) = max
End Sub

