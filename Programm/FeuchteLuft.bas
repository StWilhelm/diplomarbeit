Attribute VB_Name = "FeuchteLuft"
Function cp_trL_Br(T As Double)
'Eingabe: T in K
'Ausgabe: spez. Wärmekap. in kJ/kg/K

L_BR_1 = 1.004173
L_BR_2 = 0.0000191921
L_BR_3 = 0.00000055883483
L_BR_4 = -7.011184E-10
L_BR_5 = 3.309525E-13
L_BR_6 = -5.673876E-17

t_Br = T - 273.15

cp_trL_Br = L_BR_1 + L_BR_2 * t_Br + L_BR_3 * t_Br ^ 2 + L_BR_4 * t_Br ^ 3 + L_BR_5 * t_Br ^ 4 + L_BR_6 * t_Br ^ 5

End Function

Function h_trL_Br(T As Double)
'Eingabe: T in K
'Ausgabe: Enthalpie in kJ/kg

L_BR_1 = 1.004173
L_BR_2 = 0.0000191921
L_BR_3 = 0.00000055883483
L_BR_4 = -7.011184E-10
L_BR_5 = 3.309525E-13
L_BR_6 = -5.673876E-17

t_Br = T - 273.15

h_trL_Br = L_BR_1 * t_Br + L_BR_2 / 2 * t_Br ^ 2 + L_BR_3 / 3 * t_Br ^ 3 + L_BR_4 / 4 * t_Br ^ 4 + L_BR_5 / 5 * t_Br ^ 5 + L_BR_6 / 6 * t_Br ^ 6

End Function

Function cp_diff_Br(T As Double)
'Eingabe: T in K
'Ausgabe: Diff. spez. Wärmekap. in kJ/kg/K

L_BR_7 = 0.8648769
L_BR_8 = 0.0001825055
L_BR_9 = -0.0000005579395
L_BR_10 = 0.000000003231276
L_BR_11 = -2.753781E-12

t_Br = T - 273.15

cp_diff_Br = L_BR_7 + L_BR_8 * t_Br + L_BR_9 * t_Br ^ 2 + L_BR_10 * t_Br ^ 3 + L_BR_11 * t_Br ^ 4

End Function

Function h_diff_Br(T As Double)
'Eingabe: T in K
'Ausgabe: Diff. Enthalpie in kJ/kg

L_BR_7 = 0.8648769
L_BR_8 = 0.0001825055
L_BR_9 = -0.0000005579395
L_BR_10 = 0.000000003231276
L_BR_11 = -2.753781E-12

t_Br = T - 273.15

h_diff_Br = L_BR_7 * t_Br + L_BR_8 / 2 * t_Br ^ 2 + L_BR_9 / 3 * t_Br ^ 3 + L_BR_10 / 4 * t_Br ^ 4 + L_BR_11 / 5 * t_Br ^ 5

End Function

Function cp_fL_Br(T As Double, xw As Double) As Double
'Eingabe: T in K, xw in kg_W/kg_trL
'Ausgabe: spez. Wärmekap. in kJ/kg/K

cp_fL_Br = cp_trL_Br(T) + cp_diff_Br(T) * xw / (1 + xw)

End Function

Function h_fL_Br(T As Double, xw As Double) As Double
'Eingabe: T in K, xw in kg_W/kg_trL
'Ausgabe: Enthalpie in kJ/kg

h_fL_Br = h_trL_Br(T) + h_diff_Br(T) * xw / (1 + xw)

End Function

Function eta_trL_Br(T As Double)
'Eingabe: T in K
'Ausgabe: dyn. Visk. in mikroPa*s

L_BR_ET_1 = 17.14237
L_BR_ET_2 = 0.0463604
L_BR_ET_3 = -0.00002745836
L_BR_ET_4 = 0.00000001811235
L_BR_ET_5 = -6.74497E-12
L_BR_ET_6 = 1.027747E-15

t_Br = T - 273.15

eta_trL_Br = (L_BR_ET_1 + L_BR_ET_2 * t_Br + L_BR_ET_3 * t_Br ^ 2 + L_BR_ET_4 * t_Br ^ 3 + L_BR_ET_5 * t_Br ^ 4 + L_BR_ET_6 * t_Br ^ 5) / 1000000

End Function

Function lambda_trL_Br(T As Double)
'Eingabe: T in K
'Ausgabe: Wärmeleitfähigkeit in W/m/K

L_BR_LA_1 = 0.02498583
L_BR_LA_2 = 0.00006535367
L_BR_LA_3 = -0.000000007690843
L_BR_LA_4 = -1.924248E-12
L_BR_LA_5 = 1.60998E-15
L_BR_LA_6 = -2.86443E-19

t_Br = T - 273.15

lambda_trL_Br = L_BR_LA_1 + L_BR_LA_2 * t_Br + L_BR_LA_3 * t_Br ^ 2 + L_BR_LA_4 * t_Br ^ 3 + L_BR_LA_5 * t_Br ^ 4 + L_BR_LA_6 * t_Br ^ 5

End Function

Function eta_diff_Br(T As Double)
'Eingabe: T in K
'Ausgabe: Diff. dyn. Visk. in mikroPa*s

L_BR_ET_7 = -9.108949
L_BR_ET_8 = 0.02654355
L_BR_ET_9 = -0.00006432423
L_BR_ET_10 = 0.0000001307935
L_BR_ET_11 = -8.190284E-11

t_Br = T - 273.15

eta_diff_Br = (L_BR_ET_7 + L_BR_ET_8 * t_Br + L_BR_ET_9 * t_Br ^ 2 + L_BR_ET_10 * t_Br ^ 3 + L_BR_ET_11 * t_Br ^ 4) / 1000000

End Function

Function lambda_diff_Br(T As Double)
'Eingabe: T in K
'Ausgabe: Wärmeleitfähigkeit in W/m/K

L_BR_LA_7 = -0.01076906
L_BR_LA_8 = 0.0001043574
L_BR_LA_9 = -0.0000001269933
L_BR_LA_10 = 3.323288E-10
L_BR_LA_11 = -2.247442E-13

t_Br = T - 273.15

lambda_diff_Br = L_BR_LA_7 + L_BR_LA_8 * t_Br + L_BR_LA_9 * t_Br ^ 2 + L_BR_LA_10 * t_Br ^ 3 + L_BR_LA_11 * t_Br ^ 4

End Function

Function eta_fL_Br(T As Double, xw As Double) As Double
'Eingabe: T in K, xw in kg_W/kg_trL
'Ausgabe: dyn. Visk. in mikroPa*s

eta_fL_Br = (eta_trL_Br(T) + eta_diff_Br(T) * xw / (1 + xw))

End Function

Function lambda_fL_Br(T As Double, xw As Double) As Double
'Eingabe: T in K, xw in kg_W/kg_trL
'Ausgabe: Wärmeleitfähigkeit in W/m/K

lambda_fL_Br = lambda_trL_Br(T) + lambda_diff_Br(T) * xw / (1 + xw)

End Function

Function Pr_trL_Br(T As Double)
'Eingabe: T in K
'Ausgabe: Prandtl-Zahl

L_BR_PR_1 = 0.6901183
L_BR_PR_2 = 0.000002417094
L_BR_PR_3 = 0.00000002771383
L_BR_PR_4 = -3.534575E-11
L_BR_PR_5 = 1.71793E-14
L_BR_PR_6 = -2.989654E-18

t_Br = T - 273.15

Pr_trL_Br = L_BR_PR_1 + L_BR_PR_2 * t_Br + L_BR_PR_3 * t_Br ^ 2 + L_BR_PR_4 * t_Br ^ 3 + L_BR_PR_5 * t_Br ^ 4 + L_BR_PR_6 * t_Br ^ 5

End Function

Function rho_fL_Br(xw As Double) As Double

rho_fL_Br = 1.293 * (1 + xw) / (1 + xw * 1.6078)

End Function

Function ps_CIPM(T As Double) As Double
'Eingabe: T in K
'Ausgabe: Sättigungsdampfdruck ps in kPa

CIPM_A = 0.000012378847
CIPM_B = -0.019121316
CIPM_C = 33.93711047
CIPM_d = -6343.1645

ps_CIPM = 0.001 * Exp((CIPM_A * T ^ 2) + CIPM_B * T + CIPM_C + CIPM_d / T)

End Function

Function f_CIPM(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Überhöhungsfaktor f

CIPM_alpha = 1.00062
CIPM_beta = 0.0000000314
CIPM_gamma = 0.00000056

temp = T - 273.15

f_CIPM = CIPM_alpha + CIPM_beta * p * 1000 + CIPM_gamma * temp ^ 2

End Function

Function Z_CIPM(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_trL
'Ausgabe: Kompressibilitätsfaktor Z

CIPM_a0 = 0.00000158123
CIPM_a1 = -0.000000029331
CIPM_a2 = 0.00000000011043
CIPM_b0 = 0.000005707
CIPM_b1 = -0.00000002051
CIPM_c0 = 0.00019898
CIPM_c1 = -0.000002376
CIPM_d = 0.0000000000183
CIPM_e = -0.00000000765

temp = T - 273.15

Z_CIPM = 1 - (p * 1000 / T) * (CIPM_a0 + CIPM_a1 * temp + (CIPM_a2 * temp ^ 2) + (CIPM_b0 + CIPM_b1 * temp) * xv_CIPM(p, T, xw) + (CIPM_c0 + CIPM_c1 * temp) * (xv_CIPM(p, T, xw) ^ 2)) + (((p * 1000) ^ 2) / (T ^ 2)) * (CIPM_d + CIPM_e * (xv_CIPM(p, T, xw) ^ 2))

End Function

Function xv_CIPM(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_trL
'Ausgabe: mol. Wassergehalt nach CIPM

xv_CIPM = phi_W(p, T, xw) * f_CIPM(p, T) * ps_CIPM(T) / p

End Function

Function rho_fL_CIPM(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_trL
'Ausgabe: Dichte feuchter Luft nach CIPM-07

rho_fL_CIPM = 3.48374 * p / Z_CIPM(p, T, xw) / T * (1 - 0.378 * xv_CIPM(p, T, xw))

End Function

Function gamma_r1(p As Double, T As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K
'Ausgabe: freie Gibbs-Energie (dimensionslos)

GAM_R1_1 = 0.14632971213167
GAM_R1_2 = -0.84548187169114
GAM_R1_3 = -3.756360367204
GAM_R1_4 = 3.3855169168385
GAM_R1_5 = -0.95791963387872
GAM_R1_6 = 0.15772038513228
GAM_R1_7 = -0.016616417199501
GAM_R1_8 = 8.1214629983568E-04
GAM_R1_9 = 2.8319080123804E-04
GAM_R1_10 = -6.0706301565874E-04
GAM_R1_11 = -0.018990068218419
GAM_R1_12 = -0.032529748770505
GAM_R1_13 = -0.021841717175414
GAM_R1_14 = -5.283835796993E-05
GAM_R1_15 = -4.7184321073267E-04
GAM_R1_16 = -3.0001780793026E-04
GAM_R1_17 = 4.7661393906987E-05
GAM_R1_18 = -4.4141845330846E-06
GAM_R1_19 = -7.2694996297594E-16
GAM_R1_20 = -3.1679644845054E-05
GAM_R1_21 = -2.8270797985312E-06
GAM_R1_22 = -8.5205128120103E-10
GAM_R1_23 = -2.2425281908E-06
GAM_R1_24 = -6.5171222895601E-07
GAM_R1_25 = -1.4341729937924E-13
GAM_R1_26 = -4.0516996860117E-07
GAM_R1_27 = -1.2734301741641E-09
GAM_R1_28 = -1.7424871230634E-10
GAM_R1_29 = -6.8762131295531E-19
GAM_R1_30 = 1.4478307828521E-20
GAM_R1_31 = 2.6335781662795E-23
GAM_R1_32 = -1.1947622640071E-23
GAM_R1_33 = 1.8228094581404E-24
GAM_R1_34 = -9.3537087292458E-26

gamr1_pi = p / 16530
gamr1_tau = 1386 / T

gam_r1_s1 = GAM_R1_1 * ((7.1 - gamr1_pi) ^ 0) * ((gamr1_tau - 1.222) ^ -2) + GAM_R1_2 * ((7.1 - gamr1_pi) ^ 0) * ((gamr1_tau - 1.222) ^ -1) + GAM_R1_3 * ((7.1 - gamr1_pi) ^ 0) * ((gamr1_tau - 1.222) ^ 0) + GAM_R1_4 * ((7.1 - gamr1_pi) ^ 0) * ((gamr1_tau - 1.222) ^ 1) + GAM_R1_5 * ((7.1 - gamr1_pi) ^ 0) * ((gamr1_tau - 1.222) ^ 2) + GAM_R1_6 * ((7.1 - gamr1_pi) ^ 0) * ((gamr1_tau - 1.222) ^ 3) + GAM_R1_7 * ((7.1 - gamr1_pi) ^ 0) * ((gamr1_tau - 1.222) ^ 4) + GAM_R1_8 * ((7.1 - gamr1_pi) ^ 0) * ((gamr1_tau - 1.222) ^ 5) + GAM_R1_9 * ((7.1 - gamr1_pi) ^ 1) * ((gamr1_tau - 1.222) ^ -9) + GAM_R1_10 * ((7.1 - gamr1_pi) ^ 1) * ((gamr1_tau - 1.222) ^ -7)
gam_r1_s2 = GAM_R1_11 * ((7.1 - gamr1_pi) ^ 1) * ((gamr1_tau - 1.222) ^ -1) + GAM_R1_12 * ((7.1 - gamr1_pi) ^ 1) * ((gamr1_tau - 1.222) ^ 0) + GAM_R1_13 * ((7.1 - gamr1_pi) ^ 1) * ((gamr1_tau - 1.222) ^ 1) + GAM_R1_14 * ((7.1 - gamr1_pi) ^ 1) * ((gamr1_tau - 1.222) ^ 3) + GAM_R1_15 * ((7.1 - gamr1_pi) ^ 2) * ((gamr1_tau - 1.222) ^ -3) + GAM_R1_16 * ((7.1 - gamr1_pi) ^ 2) * ((gamr1_tau - 1.222) ^ 0) + GAM_R1_17 * ((7.1 - gamr1_pi) ^ 2) * ((gamr1_tau - 1.222) ^ 1) + GAM_R1_18 * ((7.1 - gamr1_pi) ^ 2) * ((gamr1_tau - 1.222) ^ 3) + GAM_R1_19 * ((7.1 - gamr1_pi) ^ 2) * ((gamr1_tau - 1.222) ^ 17) + GAM_R1_20 * ((7.1 - gamr1_pi) ^ 3) * ((gamr1_tau - 1.222) ^ -4)
gam_r1_s3 = GAM_R1_21 * ((7.1 - gamr1_pi) ^ 3) * ((gamr1_tau - 1.222) ^ 0) + GAM_R1_22 * ((7.1 - gamr1_pi) ^ 3) * ((gamr1_tau - 1.222) ^ 6) + GAM_R1_23 * ((7.1 - gamr1_pi) ^ 4) * ((gamr1_tau - 1.222) ^ -5) + GAM_R1_24 * ((7.1 - gamr1_pi) ^ 4) * ((gamr1_tau - 1.222) ^ -2) + GAM_R1_25 * ((7.1 - gamr1_pi) ^ 4) * ((gamr1_tau - 1.222) ^ 10) + GAM_R1_26 * ((7.1 - gamr1_pi) ^ 5) * ((gamr1_tau - 1.222) ^ -8) + GAM_R1_27 * ((7.1 - gamr1_pi) ^ 8) * ((gamr1_tau - 1.222) ^ -11) + GAM_R1_28 * ((7.1 - gamr1_pi) ^ 8) * ((gamr1_tau - 1.222) ^ -6) + GAM_R1_29 * ((7.1 - gamr1_pi) ^ 21) * ((gamr1_tau - 1.222) ^ -29) + GAM_R1_30 * ((7.1 - gamr1_pi) ^ 23) * ((gamr1_tau - 1.222) ^ -31)
gam_r1_s4 = GAM_R1_31 * ((7.1 - gamr1_pi) ^ 29) * ((gamr1_tau - 1.222) ^ -38) + GAM_R1_32 * ((7.1 - gamr1_pi) ^ 30) * ((gamr1_tau - 1.222) ^ -39) + GAM_R1_33 * ((7.1 - gamr1_pi) ^ 31) * ((gamr1_tau - 1.222) ^ -40) + GAM_R1_34 * ((7.1 - gamr1_pi) ^ 32) * ((gamr1_tau - 1.222) ^ -41)

gamma_r1 = gam_r1_s1 + gam_r1_s2 + gam_r1_s3 + gam_r1_s4

End Function

Function gamma_r1_pi(p As Double, T As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K
'Ausgabe: Ableitung der freien Gibbs-Energie nach dem Druck (dimensionslos)

GAM_R1_1 = 0.14632971213167
GAM_R1_2 = -0.84548187169114
GAM_R1_3 = -3.756360367204
GAM_R1_4 = 3.3855169168385
GAM_R1_5 = -0.95791963387872
GAM_R1_6 = 0.15772038513228
GAM_R1_7 = -0.016616417199501
GAM_R1_8 = 8.1214629983568E-04
GAM_R1_9 = 2.8319080123804E-04
GAM_R1_10 = -6.0706301565874E-04
GAM_R1_11 = -0.018990068218419
GAM_R1_12 = -0.032529748770505
GAM_R1_13 = -0.021841717175414
GAM_R1_14 = -5.283835796993E-05
GAM_R1_15 = -4.7184321073267E-04
GAM_R1_16 = -3.0001780793026E-04
GAM_R1_17 = 4.7661393906987E-05
GAM_R1_18 = -4.4141845330846E-06
GAM_R1_19 = -7.2694996297594E-16
GAM_R1_20 = -3.1679644845054E-05
GAM_R1_21 = -2.8270797985312E-06
GAM_R1_22 = -8.5205128120103E-10
GAM_R1_23 = -2.2425281908E-06
GAM_R1_24 = -6.5171222895601E-07
GAM_R1_25 = -1.4341729937924E-13
GAM_R1_26 = -4.0516996860117E-07
GAM_R1_27 = -1.2734301741641E-09
GAM_R1_28 = -1.7424871230634E-10
GAM_R1_29 = -6.8762131295531E-19
GAM_R1_30 = 1.4478307828521E-20
GAM_R1_31 = 2.6335781662795E-23
GAM_R1_32 = -1.1947622640071E-23
GAM_R1_33 = 1.8228094581404E-24
GAM_R1_34 = -9.3537087292458E-26

gamr1_pi = p / 16530
gamr1_tau = 1386 / T

gam_r1_s1_pi = -GAM_R1_1 * 0 * ((7.1 - gamr1_pi) ^ (0 - 1)) * ((gamr1_tau - 1.222) ^ -2) + -GAM_R1_2 * 0 * ((7.1 - gamr1_pi) ^ (0 - 1)) * ((gamr1_tau - 1.222) ^ -1) + -GAM_R1_3 * 0 * ((7.1 - gamr1_pi) ^ (0 - 1)) * ((gamr1_tau - 1.222) ^ 0) + -GAM_R1_4 * 0 * ((7.1 - gamr1_pi) ^ (0 - 1)) * ((gamr1_tau - 1.222) ^ 1) + -GAM_R1_5 * 0 * ((7.1 - gamr1_pi) ^ (0 - 1)) * ((gamr1_tau - 1.222) ^ 2) + -GAM_R1_6 * 0 * ((7.1 - gamr1_pi) ^ (0 - 1)) * ((gamr1_tau - 1.222) ^ 3) + -GAM_R1_7 * 0 * ((7.1 - gamr1_pi) ^ (0 - 1)) * ((gamr1_tau - 1.222) ^ 4) + -GAM_R1_8 * 0 * ((7.1 - gamr1_pi) ^ (0 - 1)) * ((gamr1_tau - 1.222) ^ 5) + -GAM_R1_9 * 1 * ((7.1 - gamr1_pi) ^ (1 - 1)) * ((gamr1_tau - 1.222) ^ -9) + -GAM_R1_10 * 1 * ((7.1 - gamr1_pi) ^ (1 - 1)) * ((gamr1_tau - 1.222) ^ -7)
gam_r1_s2_pi = -GAM_R1_11 * 1 * ((7.1 - gamr1_pi) ^ (1 - 1)) * ((gamr1_tau - 1.222) ^ -1) + -GAM_R1_12 * 1 * ((7.1 - gamr1_pi) ^ (1 - 1)) * ((gamr1_tau - 1.222) ^ 0) + -GAM_R1_13 * 1 * ((7.1 - gamr1_pi) ^ (1 - 1)) * ((gamr1_tau - 1.222) ^ 1) + -GAM_R1_14 * 1 * ((7.1 - gamr1_pi) ^ (1 - 1)) * ((gamr1_tau - 1.222) ^ 3) + -GAM_R1_15 * 2 * ((7.1 - gamr1_pi) ^ (2 - 1)) * ((gamr1_tau - 1.222) ^ -3) + -GAM_R1_16 * 2 * ((7.1 - gamr1_pi) ^ (2 - 1)) * ((gamr1_tau - 1.222) ^ 0) + -GAM_R1_17 * 2 * ((7.1 - gamr1_pi) ^ (2 - 1)) * ((gamr1_tau - 1.222) ^ 1) + -GAM_R1_18 * 2 * ((7.1 - gamr1_pi) ^ (2 - 1)) * ((gamr1_tau - 1.222) ^ 3) + -GAM_R1_19 * 2 * ((7.1 - gamr1_pi) ^ (2 - 1)) * ((gamr1_tau - 1.222) ^ 17) + -GAM_R1_20 * 3 * ((7.1 - gamr1_pi) ^ (3 - 1)) * ((gamr1_tau - 1.222) ^ -4)
gam_r1_s3_pi = -GAM_R1_21 * 3 * ((7.1 - gamr1_pi) ^ (3 - 1)) * ((gamr1_tau - 1.222) ^ 0) + -GAM_R1_22 * 3 * ((7.1 - gamr1_pi) ^ (3 - 1)) * ((gamr1_tau - 1.222) ^ 6) + -GAM_R1_23 * 4 * ((7.1 - gamr1_pi) ^ (4 - 1)) * ((gamr1_tau - 1.222) ^ -5) + -GAM_R1_24 * 4 * ((7.1 - gamr1_pi) ^ (4 - 1)) * ((gamr1_tau - 1.222) ^ -2) + -GAM_R1_25 * 4 * ((7.1 - gamr1_pi) ^ (4 - 1)) * ((gamr1_tau - 1.222) ^ 10) + -GAM_R1_26 * 5 * ((7.1 - gamr1_pi) ^ (5 - 1)) * ((gamr1_tau - 1.222) ^ -8) + -GAM_R1_27 * 8 * ((7.1 - gamr1_pi) ^ (8 - 1)) * ((gamr1_tau - 1.222) ^ -11) + -GAM_R1_28 * 8 * ((7.1 - gamr1_pi) ^ (8 - 1)) * ((gamr1_tau - 1.222) ^ -6) + -GAM_R1_29 * 21 * ((7.1 - gamr1_pi) ^ (21 - 1)) * ((gamr1_tau - 1.222) ^ -29) + -GAM_R1_30 * 23 * ((7.1 - gamr1_pi) ^ (23 - 1)) * ((gamr1_tau - 1.222) ^ -31)
gam_r1_s4_pi = -GAM_R1_31 * 29 * ((7.1 - gamr1_pi) ^ (29 - 1)) * ((gamr1_tau - 1.222) ^ -38) + -GAM_R1_32 * 30 * ((7.1 - gamr1_pi) ^ (30 - 1)) * ((gamr1_tau - 1.222) ^ -39) + -GAM_R1_33 * 31 * ((7.1 - gamr1_pi) ^ (31 - 1)) * ((gamr1_tau - 1.222) ^ -40) + -GAM_R1_34 * 32 * ((7.1 - gamr1_pi) ^ (32 - 1)) * ((gamr1_tau - 1.222) ^ -41)

gamma_r1_pi = gam_r1_s1_pi + gam_r1_s2_pi + gam_r1_s3_pi + gam_r1_s4_pi

End Function

Function gamma_r1_tau(p As Double, T As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K
'Ausgabe: Ableitung der freien Gibbs-Energie nach der Temperatur (dimensionslos)

GAM_R1_1 = 0.14632971213167
GAM_R1_2 = -0.84548187169114
GAM_R1_3 = -3.756360367204
GAM_R1_4 = 3.3855169168385
GAM_R1_5 = -0.95791963387872
GAM_R1_6 = 0.15772038513228
GAM_R1_7 = -0.016616417199501
GAM_R1_8 = 8.1214629983568E-04
GAM_R1_9 = 2.8319080123804E-04
GAM_R1_10 = -6.0706301565874E-04
GAM_R1_11 = -0.018990068218419
GAM_R1_12 = -0.032529748770505
GAM_R1_13 = -0.021841717175414
GAM_R1_14 = -5.283835796993E-05
GAM_R1_15 = -4.7184321073267E-04
GAM_R1_16 = -3.0001780793026E-04
GAM_R1_17 = 4.7661393906987E-05
GAM_R1_18 = -4.4141845330846E-06
GAM_R1_19 = -7.2694996297594E-16
GAM_R1_20 = -3.1679644845054E-05
GAM_R1_21 = -2.8270797985312E-06
GAM_R1_22 = -8.5205128120103E-10
GAM_R1_23 = -2.2425281908E-06
GAM_R1_24 = -6.5171222895601E-07
GAM_R1_25 = -1.4341729937924E-13
GAM_R1_26 = -4.0516996860117E-07
GAM_R1_27 = -1.2734301741641E-09
GAM_R1_28 = -1.7424871230634E-10
GAM_R1_29 = -6.8762131295531E-19
GAM_R1_30 = 1.4478307828521E-20
GAM_R1_31 = 2.6335781662795E-23
GAM_R1_32 = -1.1947622640071E-23
GAM_R1_33 = 1.8228094581404E-24
GAM_R1_34 = -9.3537087292458E-26

gamr1_pi = p / 16530
gamr1_tau = 1386 / T

gam_r1_s1_tau = GAM_R1_1 * ((7.1 - gamr1_pi) ^ 0) * -2 * ((gamr1_tau - 1.222) ^ (-2 - 1)) + GAM_R1_2 * ((7.1 - gamr1_pi) ^ 0) * -1 * ((gamr1_tau - 1.222) ^ (-1 - 1)) + GAM_R1_3 * ((7.1 - gamr1_pi) ^ 0) * 0 * ((gamr1_tau - 1.222) ^ (0 - 1)) + GAM_R1_4 * ((7.1 - gamr1_pi) ^ 0) * 1 * ((gamr1_tau - 1.222) ^ (1 - 1)) + GAM_R1_5 * ((7.1 - gamr1_pi) ^ 0) * 2 * ((gamr1_tau - 1.222) ^ (2 - 1)) + GAM_R1_6 * ((7.1 - gamr1_pi) ^ 0) * 3 * ((gamr1_tau - 1.222) ^ (3 - 1)) + GAM_R1_7 * ((7.1 - gamr1_pi) ^ 0) * 4 * ((gamr1_tau - 1.222) ^ (4 - 1)) + GAM_R1_8 * ((7.1 - gamr1_pi) ^ 0) * 5 * ((gamr1_tau - 1.222) ^ (5 - 1)) + GAM_R1_9 * ((7.1 - gamr1_pi) ^ 1) * -9 * ((gamr1_tau - 1.222) ^ (-9 - 1)) + GAM_R1_10 * ((7.1 - gamr1_pi) ^ 1) * -7 * ((gamr1_tau - 1.222) ^ (-7 - 1))
gam_r1_s2_tau = GAM_R1_11 * ((7.1 - gamr1_pi) ^ 1) * -1 * ((gamr1_tau - 1.222) ^ (-1 - 1)) + GAM_R1_12 * ((7.1 - gamr1_pi) ^ 1) * 0 * ((gamr1_tau - 1.222) ^ (0 - 1)) + GAM_R1_13 * ((7.1 - gamr1_pi) ^ 1) * 1 * ((gamr1_tau - 1.222) ^ (1 - 1)) + GAM_R1_14 * ((7.1 - gamr1_pi) ^ 1) * 3 * ((gamr1_tau - 1.222) ^ (3 - 1)) + GAM_R1_15 * ((7.1 - gamr1_pi) ^ 2) * -3 * ((gamr1_tau - 1.222) ^ (-3 - 1)) + GAM_R1_16 * ((7.1 - gamr1_pi) ^ 2) * 0 * ((gamr1_tau - 1.222) ^ (0 - 1)) + GAM_R1_17 * ((7.1 - gamr1_pi) ^ 2) * 1 * ((gamr1_tau - 1.222) ^ (1 - 1)) + GAM_R1_18 * ((7.1 - gamr1_pi) ^ 2) * 3 * ((gamr1_tau - 1.222) ^ (3 - 1)) + GAM_R1_19 * ((7.1 - gamr1_pi) ^ 2) * 17 * ((gamr1_tau - 1.222) ^ (17 - 1)) + GAM_R1_20 * ((7.1 - gamr1_pi) ^ 3) * -4 * ((gamr1_tau - 1.222) ^ (-4 - 1))
gam_r1_s3_tau = GAM_R1_21 * ((7.1 - gamr1_pi) ^ 3) * 0 * ((gamr1_tau - 1.222) ^ (0 - 1)) + GAM_R1_22 * ((7.1 - gamr1_pi) ^ 3) * 6 * ((gamr1_tau - 1.222) ^ (6 - 1)) + GAM_R1_23 * ((7.1 - gamr1_pi) ^ 4) * -5 * ((gamr1_tau - 1.222) ^ (-5 - 1)) + GAM_R1_24 * ((7.1 - gamr1_pi) ^ 4) * -2 * ((gamr1_tau - 1.222) ^ (-2 - 1)) + GAM_R1_25 * ((7.1 - gamr1_pi) ^ 4) * 10 * ((gamr1_tau - 1.222) ^ (10 - 1)) + GAM_R1_26 * ((7.1 - gamr1_pi) ^ 5) * -8 * ((gamr1_tau - 1.222) ^ (-8 - 1)) + GAM_R1_27 * ((7.1 - gamr1_pi) ^ 8) * -11 * ((gamr1_tau - 1.222) ^ (-11 - 1)) + GAM_R1_28 * ((7.1 - gamr1_pi) ^ 8) * -6 * ((gamr1_tau - 1.222) ^ (-6 - 1)) + GAM_R1_29 * ((7.1 - gamr1_pi) ^ 21) * -29 * ((gamr1_tau - 1.222) ^ (-29 - 1)) + GAM_R1_30 * ((7.1 - gamr1_pi) ^ 23) * -31 * ((gamr1_tau - 1.222) ^ (-31 - 1))
gam_r1_s4_tau = GAM_R1_31 * ((7.1 - gamr1_pi) ^ 29) * -38 * ((gamr1_tau - 1.222) ^ (-38 - 1)) + GAM_R1_32 * ((7.1 - gamr1_pi) ^ 30) * -39 * ((gamr1_tau - 1.222) ^ (-39 - 1)) + GAM_R1_33 * ((7.1 - gamr1_pi) ^ 31) * -40 * ((gamr1_tau - 1.222) ^ (-40 - 1)) + GAM_R1_34 * ((7.1 - gamr1_pi) ^ 32) * -41 * ((gamr1_tau - 1.222) ^ (-41 - 1))

gamma_r1_tau = gam_r1_s1_tau + gam_r1_s2_tau + gam_r1_s3_tau + gam_r1_s4_tau

End Function

Function gamma_r1_tautau(p As Double, T As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K
'Ausgabe: zweite Ableitung der freien Gibbs-Energie nach der Temperatur (dimensionslos)

GAM_R1_1 = 0.14632971213167
GAM_R1_2 = -0.84548187169114
GAM_R1_3 = -3.756360367204
GAM_R1_4 = 3.3855169168385
GAM_R1_5 = -0.95791963387872
GAM_R1_6 = 0.15772038513228
GAM_R1_7 = -0.016616417199501
GAM_R1_8 = 8.1214629983568E-04
GAM_R1_9 = 2.8319080123804E-04
GAM_R1_10 = -6.0706301565874E-04
GAM_R1_11 = -0.018990068218419
GAM_R1_12 = -0.032529748770505
GAM_R1_13 = -0.021841717175414
GAM_R1_14 = -5.283835796993E-05
GAM_R1_15 = -4.7184321073267E-04
GAM_R1_16 = -3.0001780793026E-04
GAM_R1_17 = 4.7661393906987E-05
GAM_R1_18 = -4.4141845330846E-06
GAM_R1_19 = -7.2694996297594E-16
GAM_R1_20 = -3.1679644845054E-05
GAM_R1_21 = -2.8270797985312E-06
GAM_R1_22 = -8.5205128120103E-10
GAM_R1_23 = -2.2425281908E-06
GAM_R1_24 = -6.5171222895601E-07
GAM_R1_25 = -1.4341729937924E-13
GAM_R1_26 = -4.0516996860117E-07
GAM_R1_27 = -1.2734301741641E-09
GAM_R1_28 = -1.7424871230634E-10
GAM_R1_29 = -6.8762131295531E-19
GAM_R1_30 = 1.4478307828521E-20
GAM_R1_31 = 2.6335781662795E-23
GAM_R1_32 = -1.1947622640071E-23
GAM_R1_33 = 1.8228094581404E-24
GAM_R1_34 = -9.3537087292458E-26

gamr1_pi = p / 16530
gamr1_tau = 1386 / T

gam_r1_s1_tautau = GAM_R1_1 * ((7.1 - gamr1_pi) ^ 0) * -2 * (-2 - 1) * ((gamr1_tau - 1.222) ^ (-2 - 2)) + GAM_R1_2 * ((7.1 - gamr1_pi) ^ 0) * -1 * (-1 - 1) * ((gamr1_tau - 1.222) ^ (-1 - 2)) + GAM_R1_3 * ((7.1 - gamr1_pi) ^ 0) * 0 * (0 - 1) * ((gamr1_tau - 1.222) ^ (0 - 2)) + GAM_R1_4 * ((7.1 - gamr1_pi) ^ 0) * 1 * (1 - 1) * ((gamr1_tau - 1.222) ^ (1 - 2)) + GAM_R1_5 * ((7.1 - gamr1_pi) ^ 0) * 2 * (2 - 1) * ((gamr1_tau - 1.222) ^ (2 - 2)) + GAM_R1_6 * ((7.1 - gamr1_pi) ^ 0) * 3 * (3 - 1) * ((gamr1_tau - 1.222) ^ (3 - 2)) + GAM_R1_7 * ((7.1 - gamr1_pi) ^ 0) * 4 * (4 - 1) * ((gamr1_tau - 1.222) ^ (4 - 2)) + GAM_R1_8 * ((7.1 - gamr1_pi) ^ 0) * 5 * (5 - 1) * ((gamr1_tau - 1.222) ^ (5 - 2)) + GAM_R1_9 * ((7.1 - gamr1_pi) ^ 1) * -9 * (-9 - 1) * ((gamr1_tau - 1.222) ^ (-9 - 2)) + GAM_R1_10 * ((7.1 - gamr1_pi) ^ 1) * -7 * (-7 - 1) * ((gamr1_tau - 1.222) ^ (-7 - 2))
gam_r1_s2_tautau = GAM_R1_11 * ((7.1 - gamr1_pi) ^ 1) * -1 * (-1 - 1) * ((gamr1_tau - 1.222) ^ (-1 - 2)) + GAM_R1_12 * ((7.1 - gamr1_pi) ^ 1) * 0 * (0 - 1) * ((gamr1_tau - 1.222) ^ (0 - 2)) + GAM_R1_13 * ((7.1 - gamr1_pi) ^ 1) * 1 * (1 - 1) * ((gamr1_tau - 1.222) ^ (1 - 2)) + GAM_R1_14 * ((7.1 - gamr1_pi) ^ 1) * 3 * (3 - 1) * ((gamr1_tau - 1.222) ^ (3 - 2)) + GAM_R1_15 * ((7.1 - gamr1_pi) ^ 2) * -3 * (-3 - 1) * ((gamr1_tau - 1.222) ^ (-3 - 2)) + GAM_R1_16 * ((7.1 - gamr1_pi) ^ 2) * 0 * (0 - 1) * ((gamr1_tau - 1.222) ^ (0 - 2)) + GAM_R1_17 * ((7.1 - gamr1_pi) ^ 2) * 1 * (1 - 1) * ((gamr1_tau - 1.222) ^ (1 - 2)) + GAM_R1_18 * ((7.1 - gamr1_pi) ^ 2) * 3 * (3 - 1) * ((gamr1_tau - 1.222) ^ (3 - 2)) + GAM_R1_19 * ((7.1 - gamr1_pi) ^ 2) * 17 * (17 - 1) * ((gamr1_tau - 1.222) ^ (17 - 2)) + GAM_R1_20 * ((7.1 - gamr1_pi) ^ 3) * -4 * (-4 - 1) * ((gamr1_tau - 1.222) ^ (-4 - 2))
gam_r1_s3_tautau = GAM_R1_21 * ((7.1 - gamr1_pi) ^ 3) * 0 * (0 - 1) * ((gamr1_tau - 1.222) ^ (0 - 2)) + GAM_R1_22 * ((7.1 - gamr1_pi) ^ 3) * 6 * (6 - 1) * ((gamr1_tau - 1.222) ^ (6 - 2)) + GAM_R1_23 * ((7.1 - gamr1_pi) ^ 4) * -5 * (-5 - 1) * ((gamr1_tau - 1.222) ^ (-5 - 2)) + GAM_R1_24 * ((7.1 - gamr1_pi) ^ 4) * -2 * (-2 - 1) * ((gamr1_tau - 1.222) ^ (-2 - 2)) + GAM_R1_25 * ((7.1 - gamr1_pi) ^ 4) * 10 * (10 - 1) * ((gamr1_tau - 1.222) ^ (10 - 2)) + GAM_R1_26 * ((7.1 - gamr1_pi) ^ 5) * -8 * (-8 - 1) * ((gamr1_tau - 1.222) ^ (-8 - 2)) + GAM_R1_27 * ((7.1 - gamr1_pi) ^ 8) * -11 * (-11 - 1) * ((gamr1_tau - 1.222) ^ (-11 - 2)) + GAM_R1_28 * ((7.1 - gamr1_pi) ^ 8) * -6 * (-6 - 1) * ((gamr1_tau - 1.222) ^ (-6 - 2)) + GAM_R1_29 * ((7.1 - gamr1_pi) ^ 21) * -29 * (-29 - 1) * ((gamr1_tau - 1.222) ^ (-29 - 2)) + GAM_R1_30 * ((7.1 - gamr1_pi) ^ 23) * -31 * (-31 - 1) * ((gamr1_tau - 1.222) ^ (-31 - 2))
gam_r1_s4_tautau = GAM_R1_31 * ((7.1 - gamr1_pi) ^ 29) * -38 * (-38 - 1) * ((gamr1_tau - 1.222) ^ (-38 - 2)) + GAM_R1_32 * ((7.1 - gamr1_pi) ^ 30) * -39 * (-39 - 1) * ((gamr1_tau - 1.222) ^ (-39 - 2)) + GAM_R1_33 * ((7.1 - gamr1_pi) ^ 31) * -40 * (-40 - 1) * ((gamr1_tau - 1.222) ^ (-40 - 2)) + GAM_R1_34 * ((7.1 - gamr1_pi) ^ 32) * -41 * (-41 - 1) * ((gamr1_tau - 1.222) ^ (-41 - 2))

gamma_r1_tautau = gam_r1_s1_tautau + gam_r1_s2_tautau + gam_r1_s3_tautau + gam_r1_s4_tautau

End Function

Function ps_I(T As Double) As Double
'Eingabe: Temperatur T in K
'Ausgabe: Sättigungsdampfdruck ps in kPa

ZW_N1 = 1167.0521452767
ZW_N2 = -724213.16703206
ZW_N3 = -17.073846940092
ZW_N4 = 12020.82470247
ZW_N5 = -3232555.0322333
ZW_N6 = 14.91510861353
ZW_N7 = -4823.2657361591
ZW_N8 = 405113.40542057
ZW_N9 = -0.23855557567849
ZW_N10 = 650.17534844798

ZW_TH = T + (ZW_N9 / (T - ZW_N10))

ZW_A = ZW_TH * ZW_TH + ZW_N1 * ZW_TH + ZW_N2
ZW_B = ZW_N3 * ZW_TH * ZW_TH + ZW_N4 * ZW_TH + ZW_N5
ZW_C = ZW_N6 * ZW_TH * ZW_TH + ZW_N7 * ZW_TH + ZW_N8

ps_I = ((2 * ZW_C / (-ZW_B + (ZW_B * ZW_B - 4 * ZW_A * ZW_C) ^ (1 / 2))) ^ 4) * 1000

End Function

Function Ts_I(p As Double) As Double
'Eingabe: Druck in kPa
'Ausgabe: Sättigungsdampftemperatur in K

ZW_N1 = 1167.0521452767
ZW_N2 = -724213.16703206
ZW_N3 = -17.073846940092
ZW_N4 = 12020.82470247
ZW_N5 = -3232555.0322333
ZW_N6 = 14.91510861353
ZW_N7 = -4823.2657361591
ZW_N8 = 405113.40542057
ZW_N9 = -0.23855557567849
ZW_N10 = 650.17534844798

ZW_BE = (p / 1000) ^ 0.25

ZW_E = ZW_BE * ZW_BE + ZW_N3 * ZW_BE + ZW_N6
ZW_F = ZW_N1 * ZW_BE * ZW_BE + ZW_N4 * ZW_BE + ZW_N7
ZW_G = ZW_N2 * ZW_BE * ZW_BE + ZW_N5 * ZW_BE + ZW_N8

ZW_D = 2 * ZW_G / (-ZW_F - (ZW_F * ZW_F - 4 * ZW_E * ZW_G) ^ 0.5)

Ts_I = 0.5 * (ZW_N10 + ZW_D - (((ZW_N10 + ZW_D) ^ 2) - 4 * (ZW_N9 + ZW_N10 * ZW_D)) ^ 0.5)

End Function

Function psubl(T As Double) As Double
'Eingabe: Temperatur T in K
'Ausgabe: Sublimationsdruck p_subl in kPa

T_Stern = 273.16
p_Stern = 0.611657
A_1_subl = -21.2144006
A_2_subl = 27.3203819
A_3_subl = -6.1059813
B_1_subl = 0.00333333333
B_2_subl = 1.20666667
B_3_subl = 1.70333333

ZW_TAU = T / T_Stern

ZW_subl_SUMME = (A_1_subl * ZW_TAU ^ B_1_subl) + (A_2_subl * ZW_TAU ^ B_2_subl) + (A_3_subl * ZW_TAU ^ B_3_subl)

ZW_subl_PROD = (1 / ZW_TAU) * ZW_subl_SUMME

subl_fit1 = -7.15295861319909E-07
subl_fit2 = 5.64921130231549E-04
subl_fit3 = -0.148504068396705
subl_fit4 = 13.9923179373957

subl_fit = subl_fit1 * T ^ 3 + subl_fit2 * T ^ 2 + subl_fit3 * T + subl_fit4

psubl = p_Stern * Exp(ZW_subl_PROD) * subl_fit

End Function

Function gamma_0(p As Double, T As Double) As Double
'Eingabe: T in K
'Ausgabe: Idealgas-Gibbs-Energie

g0_1 = -9.6927686500217
g0_2 = 10.086655968018
g0_3 = -0.005608791128302
g0_4 = 0.071452738081455
g0_5 = -0.40710498223928
g0_6 = 1.4240819171444
g0_7 = -4.383951131945
g0_8 = -0.28408632460772
g0_9 = 0.021268463753307

tau = 540 / T
pi_g = p / 1000

gamma_0 = Log(pi_g) + g0_1 * tau ^ (0) + g0_2 * tau ^ (1) + g0_3 * tau ^ (-5) + g0_4 * tau ^ (-4) + g0_5 * tau ^ (-3) + g0_6 * tau ^ (-2) + g0_7 * tau ^ (-1) + g0_8 * tau ^ (2) + g0_9 * tau ^ (3)

End Function

Function gamma_0_t(T As Double) As Double
'Eingabe: T in K
'Ausgabe: erste Ableitung der Idealgas-Gibbs-Energie nach tau

g0tt_1 = -9.6927686500217
g0tt_2 = 10.086655968018
g0tt_3 = -0.005608791128302
g0tt_4 = 0.071452738081455
g0tt_5 = -0.40710498223928
g0tt_6 = 1.4240819171444
g0tt_7 = -4.383951131945
g0tt_8 = -0.28408632460772
g0tt_9 = 0.021268463753307

tau = 540 / T

gamma_0_t = (g0tt_1 * 0 * tau ^ (0 - 1)) + (g0tt_2 * 1 * tau ^ (1 - 1)) + (g0tt_3 * -5 * tau ^ (-5 - 1)) + (g0tt_4 * -4 * tau ^ (-4 - 1)) + (g0tt_5 * -3 * tau ^ (-3 - 1)) + (g0tt_6 * -2 * tau ^ (-2 - 1)) + (g0tt_7 * -1 * tau ^ (-1 - 1)) + (g0tt_8 * 2 * tau ^ (2 - 1)) + (g0tt_9 * 3 * tau ^ (3 - 1))

End Function

Function gamma_0_tt(T As Double) As Double
'Eingabe: T in K
'Ausgabe: zweite Ableitung der Idealgas-Gibbs-Energie nach tau

g0tt_1 = -9.6927686500217
g0tt_2 = 10.086655968018
g0tt_3 = -0.005608791128302
g0tt_4 = 0.071452738081455
g0tt_5 = -0.40710498223928
g0tt_6 = 1.4240819171444
g0tt_7 = -4.383951131945
g0tt_8 = -0.28408632460772
g0tt_9 = 0.021268463753307

tau = 540 / T

gamma_0_tt = g0tt_1 * 0 * (0 - 1) * tau ^ (0 - 2) + g0tt_2 * 1 * (1 - 1) * tau ^ (1 - 2) + g0tt_3 * -5 * (-5 - 1) * tau ^ (-5 - 2) + g0tt_4 * -4 * (-4 - 1) * tau ^ (-4 - 2) + g0tt_5 * -3 * (-3 - 1) * tau ^ (-3 - 2) + g0tt_6 * -2 * (-2 - 1) * tau ^ (-2 - 2) + g0tt_7 * -1 * (-1 - 1) * tau ^ (-1 - 2) + g0tt_8 * 2 * (2 - 1) * tau ^ (2 - 2) + g0tt_9 * 3 * (3 - 1) * tau ^ (3 - 2)

End Function

Function gamma_r(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: residuale Gibbs-Energie

gr_1 = -1.7731742473213E-03
gr_2 = -0.017834862292358
gr_3 = -0.045996013696365
gr_4 = -0.057581259083432
gr_5 = -0.05032527872793
gr_6 = -3.3032641670203E-05
gr_7 = -1.8948987516315E-04
gr_8 = -3.9392777243355E-03
gr_9 = -0.043797295650573
gr_10 = -2.6674547914087E-05
gr_11 = 2.0481737692309E-08
gr_12 = 4.3870667284435E-07
gr_13 = -3.227767723857E-05
gr_14 = -1.5033924542148E-03
gr_15 = -0.040668253562649
gr_16 = -7.8847309559367E-10
gr_17 = 1.2790717852285E-08
gr_18 = 4.8225372718507E-07
gr_19 = 2.2922076337661E-06
gr_20 = -1.6714766451061E-11
gr_21 = -2.1171472321355E-03
gr_22 = -23.895741934104
gr_23 = -5.905956432427E-18
gr_24 = -1.2621808899101E-06
gr_25 = -0.038946842435739
gr_26 = 1.1256211360459E-11
gr_27 = -8.2311340897998
gr_28 = 1.9809712802088E-08
gr_29 = 1.0406965210174E-19
gr_30 = -1.0234747095929E-13
gr_31 = -1.0018179379511E-09
gr_32 = -8.0882908646985E-11
gr_33 = 0.10693031879409
gr_34 = -0.33662250574171
gr_35 = 8.9185845355421E-25
gr_36 = 3.0629316876232E-13
gr_37 = -4.2002467698208E-06
gr_38 = -5.9056029685639E-26
gr_39 = 3.7826947613457E-06
gr_40 = -1.2768608934681E-15
gr_41 = 7.3087610595061E-29
gr_42 = 5.5414715350778E-17
gr_43 = -9.436970724121E-07

pi_g = p / 1000
tau = 540 / T

SUMME_gr_1 = (gr_1 * (pi_g ^ (1)) * (tau - 0.5) ^ 0) + (gr_2 * (pi_g ^ (1)) * (tau - 0.5) ^ 1) + (gr_3 * (pi_g ^ (1)) * (tau - 0.5) ^ 2) + (gr_4 * (pi_g ^ (1)) * (tau - 0.5) ^ 3) + (gr_5 * (pi_g ^ (1)) * (tau - 0.5) ^ 6) + (gr_6 * (pi_g ^ (2)) * (tau - 0.5) ^ 1) + (gr_7 * (pi_g ^ (2)) * (tau - 0.5) ^ 2) + (gr_8 * (pi_g ^ (2)) * (tau - 0.5) ^ 4) + (gr_9 * (pi_g ^ (2)) * (tau - 0.5) ^ 7) + (gr_10 * (pi_g ^ (2)) * (tau - 0.5) ^ 36)
SUMME_gr_2 = (gr_11 * (pi_g ^ (3)) * (tau - 0.5) ^ 0) + (gr_12 * (pi_g ^ (3)) * (tau - 0.5) ^ 1) + (gr_13 * (pi_g ^ (3)) * (tau - 0.5) ^ 3) + (gr_14 * (pi_g ^ (3)) * (tau - 0.5) ^ 6) + (gr_15 * (pi_g ^ (3)) * (tau - 0.5) ^ 35) + (gr_16 * (pi_g ^ (4)) * (tau - 0.5) ^ 1) + (gr_17 * (pi_g ^ (4)) * (tau - 0.5) ^ 2) + (gr_18 * (pi_g ^ (4)) * (tau - 0.5) ^ 3) + (gr_19 * (pi_g ^ (5)) * (tau - 0.5) ^ 7) + (gr_20 * (pi_g ^ (6)) * (tau - 0.5) ^ 3)
SUMME_gr_3 = (gr_21 * (pi_g ^ (6)) * (tau - 0.5) ^ 16) + (gr_22 * (pi_g ^ (6)) * (tau - 0.5) ^ 35) + (gr_23 * (pi_g ^ (7)) * (tau - 0.5) ^ 0) + (gr_24 * (pi_g ^ (7)) * (tau - 0.5) ^ 11) + (gr_25 * (pi_g ^ (7)) * (tau - 0.5) ^ 25) + (gr_26 * (pi_g ^ (8)) * (tau - 0.5) ^ 8) + (gr_27 * (pi_g ^ (8)) * (tau - 0.5) ^ 36) + (gr_28 * (pi_g ^ (9)) * (tau - 0.5) ^ 13) + (gr_29 * (pi_g ^ (10)) * (tau - 0.5) ^ 4) + (gr_30 * (pi_g ^ (10)) * (tau - 0.5) ^ 10)
SUMME_gr_4 = (gr_31 * (pi_g ^ (10)) * (tau - 0.5) ^ 14) + (gr_32 * (pi_g ^ (16)) * (tau - 0.5) ^ 29) + (gr_33 * (pi_g ^ (16)) * (tau - 0.5) ^ 50) + (gr_34 * (pi_g ^ (18)) * (tau - 0.5) ^ 57) + (gr_35 * (pi_g ^ (20)) * (tau - 0.5) ^ 20) + (gr_36 * (pi_g ^ (20)) * (tau - 0.5) ^ 35) + (gr_37 * (pi_g ^ (20)) * (tau - 0.5) ^ 48) + (gr_38 * (pi_g ^ (21)) * (tau - 0.5) ^ 21) + (gr_39 * (pi_g ^ (22)) * (tau - 0.5) ^ 53) + (gr_40 * (pi_g ^ (23)) * (tau - 0.5) ^ 39)
SUMME_gr_5 = (gr_41 * (pi_g ^ (24)) * (tau - 0.5) ^ 26) + (gr_42 * (pi_g ^ (24)) * (tau - 0.5) ^ 40) + (gr_43 * (pi_g ^ (24)) * (tau - 0.5) ^ 58)

gamma_r = SUMME_gr_1 + SUMME_gr_2 + SUMME_gr_3 + SUMME_gr_4 + SUMME_gr_5

End Function

Function gamma_r_t(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: erste Ableitung der residualen Gibbs-Energie nach tau

grtt1 = -1.7731742473213E-03
grtt2 = -0.017834862292358
grtt3 = -0.045996013696365
grtt4 = -0.057581259083432
grtt5 = -0.05032527872793
grtt6 = -3.3032641670203E-05
grtt7 = -1.8948987516315E-04
grtt8 = -3.9392777243355E-03
grtt9 = -0.043797295650573
grtt10 = -2.6674547914087E-05
grtt11 = 2.0481737692309E-08
grtt12 = 4.3870667284435E-07
grtt13 = -3.227767723857E-05
grtt14 = -1.5033924542148E-03
grtt15 = -0.040668253562649
grtt16 = -7.8847309559367E-10
grtt17 = 1.2790717852285E-08
grtt18 = 4.8225372718507E-07
grtt19 = 2.2922076337661E-06
grtt20 = -1.6714766451061E-11
grtt21 = -2.1171472321355E-03
grtt22 = -23.895741934104
grtt23 = -5.905956432427E-18
grtt24 = -1.2621808899101E-06
grtt25 = -0.038946842435739
grtt26 = 1.1256211360459E-11
grtt27 = -8.2311340897998
grtt28 = 1.9809712802088E-08
grtt29 = 1.0406965210174E-19
grtt30 = -1.0234747095929E-13
grtt31 = -1.0018179379511E-09
grtt32 = -8.0882908646985E-11
grtt33 = 0.10693031879409
grtt34 = -0.33662250574171
grtt35 = 8.9185845355421E-25
grtt36 = 3.0629316876232E-13
grtt37 = -4.2002467698208E-06
grtt38 = -5.9056029685639E-26
grtt39 = 3.7826947613457E-06
grtt40 = -1.2768608934681E-15
grtt41 = 7.3087610595061E-29
grtt42 = 5.5414715350778E-17
grtt43 = -9.436970724121E-07

pi_g = p / 1000
tau = 540 / T

SUMME_grt_1 = (grtt1 * (pi_g ^ 1) * 0 * (tau - 0.5) ^ (0 - 1)) + (grtt2 * (pi_g ^ 1) * 1 * (tau - 0.5) ^ (1 - 1)) + (grtt3 * (pi_g ^ 1) * 2 * (tau - 0.5) ^ (2 - 1)) + (grtt4 * (pi_g ^ 1) * 3 * (tau - 0.5) ^ (3 - 1)) + (grtt5 * (pi_g ^ 1) * 6 * (tau - 0.5) ^ (6 - 1)) + (grtt6 * (pi_g ^ 2) * 1 * (tau - 0.5) ^ (1 - 1)) + (grtt7 * (pi_g ^ 2) * 2 * (tau - 0.5) ^ (2 - 1)) + (grtt8 * (pi_g ^ 2) * 4 * (tau - 0.5) ^ (4 - 1)) + (grtt9 * (pi_g ^ 2) * 7 * (tau - 0.5) ^ (7 - 1)) + (grtt10 * (pi_g ^ 2) * 36 * (tau - 0.5) ^ (36 - 1))
SUMME_grt_2 = (grtt11 * (pi_g ^ 3) * 0 * (tau - 0.5) ^ (0 - 1)) + (grtt12 * (pi_g ^ 3) * 1 * (tau - 0.5) ^ (1 - 1)) + (grtt13 * (pi_g ^ 3) * 3 * (tau - 0.5) ^ (3 - 1)) + (grtt14 * (pi_g ^ 3) * 6 * (tau - 0.5) ^ (6 - 1)) + (grtt15 * (pi_g ^ 3) * 35 * (tau - 0.5) ^ (35 - 1)) + (grtt16 * (pi_g ^ 4) * 1 * (tau - 0.5) ^ (1 - 1)) + (grtt17 * (pi_g ^ 4) * 2 * (tau - 0.5) ^ (2 - 1)) + (grtt18 * (pi_g ^ 4) * 3 * (tau - 0.5) ^ (3 - 1)) + (grtt19 * (pi_g ^ 5) * 7 * (tau - 0.5) ^ (7 - 1)) + (grtt20 * (pi_g ^ 6) * 3 * (tau - 0.5) ^ (3 - 1))
SUMME_grt_3 = (grtt21 * (pi_g ^ 6) * 16 * (tau - 0.5) ^ (16 - 1)) + (grtt22 * (pi_g ^ 6) * 35 * (tau - 0.5) ^ (35 - 1)) + (grtt23 * (pi_g ^ 7) * 0 * (tau - 0.5) ^ (0 - 1)) + (grtt24 * (pi_g ^ 7) * 11 * (tau - 0.5) ^ (11 - 1)) + (grtt25 * (pi_g ^ 7) * 25 * (tau - 0.5) ^ (25 - 1)) + (grtt26 * (pi_g ^ 8) * 8 * (tau - 0.5) ^ (8 - 1)) + (grtt27 * (pi_g ^ 8) * 36 * (tau - 0.5) ^ (36 - 1)) + (grtt28 * (pi_g ^ 9) * 13 * (tau - 0.5) ^ (13 - 1)) + (grtt29 * (pi_g ^ 10) * 4 * (tau - 0.5) ^ (4 - 1)) + (grtt30 * (pi_g ^ 10) * 10 * (tau - 0.5) ^ (10 - 1))
SUMME_grt_4 = (grtt31 * (pi_g ^ 10) * 14 * (tau - 0.5) ^ (14 - 1)) + (grtt32 * (pi_g ^ 16) * 29 * (tau - 0.5) ^ (29 - 1)) + (grtt33 * (pi_g ^ 16) * 50 * (tau - 0.5) ^ (50 - 1)) + (grtt34 * (pi_g ^ 18) * 57 * (tau - 0.5) ^ (57 - 1)) + (grtt35 * (pi_g ^ 20) * 20 * (tau - 0.5) ^ (20 - 1)) + (grtt36 * (pi_g ^ 20) * 35 * (tau - 0.5) ^ (35 - 1)) + (grtt37 * (pi_g ^ 20) * 48 * (tau - 0.5) ^ (48 - 1)) + (grtt38 * (pi_g ^ 21) * 21 * (tau - 0.5) ^ (21 - 1)) + (grtt39 * (pi_g ^ 22) * 53 * (tau - 0.5) ^ (53 - 1)) + (grtt40 * (pi_g ^ 23) * 39 * (tau - 0.5) ^ (39 - 1))
SUMME_grt_5 = (grtt41 * (pi_g ^ 24) * 26 * (tau - 0.5) ^ (26 - 1)) + (grtt42 * (pi_g ^ 24) * 40 * (tau - 0.5) ^ (40 - 1)) + (grtt43 * (pi_g ^ 24) * 58 * (tau - 0.5) ^ (58 - 1))

gamma_r_t = SUMME_grt_1 + SUMME_grt_2 + SUMME_grt_3 + SUMME_grt_4 + SUMME_grt_5

End Function

Function gamma_r_tt(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: zweite Ableitung der residualen Gibbs-Energie nach tau

grtt1 = -1.7731742473213E-03
grtt2 = -0.017834862292358
grtt3 = -0.045996013696365
grtt4 = -0.057581259083432
grtt5 = -0.05032527872793
grtt6 = -3.3032641670203E-05
grtt7 = -1.8948987516315E-04
grtt8 = -3.9392777243355E-03
grtt9 = -0.043797295650573
grtt10 = -2.6674547914087E-05
grtt11 = 2.0481737692309E-08
grtt12 = 4.3870667284435E-07
grtt13 = -3.227767723857E-05
grtt14 = -1.5033924542148E-03
grtt15 = -0.040668253562649
grtt16 = -7.8847309559367E-10
grtt17 = 1.2790717852285E-08
grtt18 = 4.8225372718507E-07
grtt19 = 2.2922076337661E-06
grtt20 = -1.6714766451061E-11
grtt21 = -2.1171472321355E-03
grtt22 = -23.895741934104
grtt23 = -5.905956432427E-18
grtt24 = -1.2621808899101E-06
grtt25 = -0.038946842435739
grtt26 = 1.1256211360459E-11
grtt27 = -8.2311340897998
grtt28 = 1.9809712802088E-08
grtt29 = 1.0406965210174E-19
grtt30 = -1.0234747095929E-13
grtt31 = -1.0018179379511E-09
grtt32 = -8.0882908646985E-11
grtt33 = 0.10693031879409
grtt34 = -0.33662250574171
grtt35 = 8.9185845355421E-25
grtt36 = 3.0629316876232E-13
grtt37 = -4.2002467698208E-06
grtt38 = -5.9056029685639E-26
grtt39 = 3.7826947613457E-06
grtt40 = -1.2768608934681E-15
grtt41 = 7.3087610595061E-29
grtt42 = 5.5414715350778E-17
grtt43 = -9.436970724121E-07

pi_g = p / 1000
tau = 540 / T

SUMME_grtt_1 = (grtt1 * (pi_g ^ 1) * 0 * (0 - 1) * (tau - 0.5) ^ (0 - 2)) + (grtt2 * (pi_g ^ 1) * 1 * (1 - 1) * (tau - 0.5) ^ (1 - 2)) + (grtt3 * (pi_g ^ 1) * 2 * (2 - 1) * (tau - 0.5) ^ (2 - 2)) + (grtt4 * (pi_g ^ 1) * 3 * (3 - 1) * (tau - 0.5) ^ (3 - 2)) + (grtt5 * (pi_g ^ 1) * 6 * (6 - 1) * (tau - 0.5) ^ (6 - 2)) + (grtt6 * (pi_g ^ 2) * 1 * (1 - 1) * (tau - 0.5) ^ (1 - 2)) + (grtt7 * (pi_g ^ 2) * 2 * (2 - 1) * (tau - 0.5) ^ (2 - 2)) + (grtt8 * (pi_g ^ 2) * 4 * (4 - 1) * (tau - 0.5) ^ (4 - 2)) + (grtt9 * (pi_g ^ 2) * 7 * (7 - 1) * (tau - 0.5) ^ (7 - 2)) + (grtt10 * (pi_g ^ 2) * 36 * (36 - 1) * (tau - 0.5) ^ (36 - 2))
SUMME_grtt_2 = (grtt11 * (pi_g ^ 3) * 0 * (0 - 1) * (tau - 0.5) ^ (0 - 2)) + (grtt12 * (pi_g ^ 3) * 1 * (1 - 1) * (tau - 0.5) ^ (1 - 2)) + (grtt13 * (pi_g ^ 3) * 3 * (3 - 1) * (tau - 0.5) ^ (3 - 2)) + (grtt14 * (pi_g ^ 3) * 6 * (6 - 1) * (tau - 0.5) ^ (6 - 2)) + (grtt15 * (pi_g ^ 3) * 35 * (35 - 1) * (tau - 0.5) ^ (35 - 2)) + (grtt16 * (pi_g ^ 4) * 1 * (1 - 1) * (tau - 0.5) ^ (1 - 2)) + (grtt17 * (pi_g ^ 4) * 2 * (2 - 1) * (tau - 0.5) ^ (2 - 2)) + (grtt18 * (pi_g ^ 4) * 3 * (3 - 1) * (tau - 0.5) ^ (3 - 2)) + (grtt19 * (pi_g ^ 5) * 7 * (7 - 1) * (tau - 0.5) ^ (7 - 2)) + (grtt20 * (pi_g ^ 6) * 3 * (3 - 1) * (tau - 0.5) ^ (3 - 2))
SUMME_grtt_3 = (grtt21 * (pi_g ^ 6) * 16 * (16 - 1) * (tau - 0.5) ^ (16 - 2)) + (grtt22 * (pi_g ^ 6) * 35 * (35 - 1) * (tau - 0.5) ^ (35 - 2)) + (grtt23 * (pi_g ^ 7) * 0 * (0 - 1) * (tau - 0.5) ^ (0 - 2)) + (grtt24 * (pi_g ^ 7) * 11 * (11 - 1) * (tau - 0.5) ^ (11 - 2)) + (grtt25 * (pi_g ^ 7) * 25 * (25 - 1) * (tau - 0.5) ^ (25 - 2)) + (grtt26 * (pi_g ^ 8) * 8 * (8 - 1) * (tau - 0.5) ^ (8 - 2)) + (grtt27 * (pi_g ^ 8) * 36 * (36 - 1) * (tau - 0.5) ^ (36 - 2)) + (grtt28 * (pi_g ^ 9) * 13 * (13 - 1) * (tau - 0.5) ^ (13 - 2)) + (grtt29 * (pi_g ^ 10) * 4 * (4 - 1) * (tau - 0.5) ^ (4 - 2)) + (grtt30 * (pi_g ^ 10) * 10 * (10 - 1) * (tau - 0.5) ^ (10 - 2))
SUMME_grtt_4 = (grtt31 * (pi_g ^ 10) * 14 * (14 - 1) * (tau - 0.5) ^ (14 - 2)) + (grtt32 * (pi_g ^ 16) * 29 * (29 - 1) * (tau - 0.5) ^ (29 - 2)) + (grtt33 * (pi_g ^ 16) * 50 * (50 - 1) * (tau - 0.5) ^ (50 - 2)) + (grtt34 * (pi_g ^ 18) * 57 * (57 - 1) * (tau - 0.5) ^ (57 - 2)) + (grtt35 * (pi_g ^ 20) * 20 * (20 - 1) * (tau - 0.5) ^ (20 - 2)) + (grtt36 * (pi_g ^ 20) * 35 * (35 - 1) * (tau - 0.5) ^ (35 - 2)) + (grtt37 * (pi_g ^ 20) * 48 * (48 - 1) * (tau - 0.5) ^ (48 - 2)) + (grtt38 * (pi_g ^ 21) * 21 * (21 - 1) * (tau - 0.5) ^ (21 - 2)) + (grtt39 * (pi_g ^ 22) * 53 * (53 - 1) * (tau - 0.5) ^ (53 - 2)) + (grtt40 * (pi_g ^ 23) * 39 * (39 - 1) * (tau - 0.5) ^ (39 - 2))
SUMME_grtt_5 = (grtt41 * (pi_g ^ 24) * 26 * (26 - 1) * (tau - 0.5) ^ (26 - 2)) + (grtt42 * (pi_g ^ 24) * 40 * (40 - 1) * (tau - 0.5) ^ (40 - 2)) + (grtt43 * (pi_g ^ 24) * 58 * (58 - 1) * (tau - 0.5) ^ (58 - 2))

gamma_r_tt = SUMME_grtt_1 + SUMME_grtt_2 + SUMME_grtt_3 + SUMME_grtt_4 + SUMME_grtt_5

End Function

Function gamma_r_pi(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Ableitung der residualen Gibbs-Energie nach pi

grpi1 = -1.7731742473213E-03
grpi2 = -0.017834862292358
grpi3 = -0.045996013696365
grpi4 = -0.057581259083432
grpi5 = -0.05032527872793
grpi6 = -3.3032641670203E-05
grpi7 = -1.8948987516315E-04
grpi8 = -3.9392777243355E-03
grpi9 = -0.043797295650573
grpi10 = -2.6674547914087E-05
grpi11 = 2.0481737692309E-08
grpi12 = 4.3870667284435E-07
grpi13 = -3.227767723857E-05
grpi14 = -1.5033924542148E-03
grpi15 = -0.040668253562649
grpi16 = -7.8847309559367E-10
grpi17 = 1.2790717852285E-08
grpi18 = 4.8225372718507E-07
grpi19 = 2.2922076337661E-06
grpi20 = -1.6714766451061E-11
grpi21 = -2.1171472321355E-03
grpi22 = -23.895741934104
grpi23 = -5.905956432427E-18
grpi24 = -1.2621808899101E-06
grpi25 = -0.038946842435739
grpi26 = 1.1256211360459E-11
grpi27 = -8.2311340897998
grpi28 = 1.9809712802088E-08
grpi29 = 1.0406965210174E-19
grpi30 = -1.0234747095929E-13
grpi31 = -1.0018179379511E-09
grpi32 = -8.0882908646985E-11
grpi33 = 0.10693031879409
grpi34 = -0.33662250574171
grpi35 = 8.9185845355421E-25
grpi36 = 3.0629316876232E-13
grpi37 = -4.2002467698208E-06
grpi38 = -5.9056029685639E-26
grpi39 = 3.7826947613457E-06
grpi40 = -1.2768608934681E-15
grpi41 = 7.3087610595061E-29
grpi42 = 5.5414715350778E-17
grpi43 = -9.436970724121E-07

pi_g = p / 1000
tau = 540 / T

SUMME_grpi_1 = (grpi1 * 1 * (pi_g ^ (1 - 1)) * (tau - 0.5) ^ 0) + (grpi2 * 1 * (pi_g ^ (1 - 1)) * (tau - 0.5) ^ 1) + (grpi3 * 1 * (pi_g ^ (1 - 1)) * (tau - 0.5) ^ 2) + (grpi4 * 1 * (pi_g ^ (1 - 1)) * (tau - 0.5) ^ 3) + (grpi5 * 1 * (pi_g ^ (1 - 1)) * (tau - 0.5) ^ 6) + (grpi6 * 2 * (pi_g ^ (2 - 1)) * (tau - 0.5) ^ 1) + (grpi7 * 2 * (pi_g ^ (2 - 1)) * (tau - 0.5) ^ 2) + (grpi8 * 2 * (pi_g ^ (2 - 1)) * (tau - 0.5) ^ 4) + (grpi9 * 2 * (pi_g ^ (2 - 1)) * (tau - 0.5) ^ 7) + (grpi10 * 2 * (pi_g ^ (2 - 1)) * (tau - 0.5) ^ 36)
SUMME_grpi_2 = (grpi11 * 3 * (pi_g ^ (3 - 1)) * (tau - 0.5) ^ 0) + (grpi12 * 3 * (pi_g ^ (3 - 1)) * (tau - 0.5) ^ 1) + (grpi13 * 3 * (pi_g ^ (3 - 1)) * (tau - 0.5) ^ 3) + (grpi14 * 3 * (pi_g ^ (3 - 1)) * (tau - 0.5) ^ 6) + (grpi15 * 3 * (pi_g ^ (3 - 1)) * (tau - 0.5) ^ 35) + (grpi16 * 4 * (pi_g ^ (4 - 1)) * (tau - 0.5) ^ 1) + (grpi17 * 4 * (pi_g ^ (4 - 1)) * (tau - 0.5) ^ 2) + (grpi18 * 4 * (pi_g ^ (4 - 1)) * (tau - 0.5) ^ 3) + (grpi19 * 5 * (pi_g ^ (5 - 1)) * (tau - 0.5) ^ 7) + (grpi20 * 6 * (pi_g ^ (6 - 1)) * (tau - 0.5) ^ 3)
SUMME_grpi_3 = (grpi21 * 6 * (pi_g ^ (6 - 1)) * (tau - 0.5) ^ 16) + (grpi22 * 6 * (pi_g ^ (6 - 1)) * (tau - 0.5) ^ 35) + (grpi23 * 7 * (pi_g ^ (7 - 1)) * (tau - 0.5) ^ 0) + (grpi24 * 7 * (pi_g ^ (7 - 1)) * (tau - 0.5) ^ 11) + (grpi25 * 7 * (pi_g ^ (7 - 1)) * (tau - 0.5) ^ 25) + (grpi26 * 8 * (pi_g ^ (8 - 1)) * (tau - 0.5) ^ 8) + (grpi27 * 8 * (pi_g ^ (8 - 1)) * (tau - 0.5) ^ 36) + (grpi28 * 9 * (pi_g ^ (9 - 1)) * (tau - 0.5) ^ 13) + (grpi29 * 10 * (pi_g ^ (10 - 1)) * (tau - 0.5) ^ 4) + (grpi30 * 10 * (pi_g ^ (10 - 1)) * (tau - 0.5) ^ 10)
SUMME_grpi_4 = (grpi31 * 10 * (pi_g ^ (10 - 1)) * (tau - 0.5) ^ 14) + (grpi32 * 16 * (pi_g ^ (16 - 1)) * (tau - 0.5) ^ 29) + (grpi33 * 16 * (pi_g ^ (16 - 1)) * (tau - 0.5) ^ 50) + (grpi34 * 18 * (pi_g ^ (18 - 1)) * (tau - 0.5) ^ 57) + (grpi35 * 20 * (pi_g ^ (20 - 1)) * (tau - 0.5) ^ 20) + (grpi36 * 20 * (pi_g ^ (20 - 1)) * (tau - 0.5) ^ 35) + (grpi37 * 20 * (pi_g ^ (20 - 1)) * (tau - 0.5) ^ 48) + (grpi38 * 21 * (pi_g ^ (21 - 1)) * (tau - 0.5) ^ 21) + (grpi39 * 22 * (pi_g ^ (22 - 1)) * (tau - 0.5) ^ 53) + (grpi40 * 23 * (pi_g ^ (23 - 1)) * (tau - 0.5) ^ 39)
SUMME_grpi_5 = (grpi41 * 24 * (pi_g ^ (24 - 1)) * (tau - 0.5) ^ 26) + (grpi42 * 24 * (pi_g ^ (24 - 1)) * (tau - 0.5) ^ 40) + (grpi43 * 24 * (pi_g ^ (24 - 1)) * (tau - 0.5) ^ 58)

gamma_r_pi = SUMME_grpi_1 + SUMME_grpi_2 + SUMME_grpi_3 + SUMME_grpi_4 + SUMME_grpi_5

End Function

Function cp_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spezifische isobare Wärmekapazität von Wasserdampf in kJ/kg/K

R_W = 0.461526
tau = 540 / T

If p < 0 Then

    cp_W = -1

Else:

    If p > ps(T) Then 'Automatisches Umschalten auf Wasser

        cp_W = cp_W_r1(p, T)
    
    Else: cp_W = -tau ^ 2 * R_W * (gamma_0_tt(T) + gamma_r_tt(p, T))
    
    End If

End If

End Function

Function h_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spezifische Enthalpie von Wasserdampf in kJ/kg

R_W = 0.461526
tau = 540 / T

If p < 0 Then

    h_W = -1

Else:

    If p > ps(T) Then 'Automatisches Umschalten auf Wasser

        h_W = h_W_r1(p, T)
    
    Else: h_W = tau * R_W * T * (gamma_0_t(T) + gamma_r_t(p, T))

    End If

End If

End Function

Function s_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spezifische Entropie von Wasserdampf in kJ/kg

R_W = 0.461526
tau = 540 / T

If p < 0 Then

    s_W = -1

Else:

    If p > ps(T) Then 'Automatisches Umschalten auf Wasser

        s_W = s_W_r1(p, T)

    Else: s_W = R_W * (tau * (gamma_0_t(T) + gamma_r_t(p, T)) - (gamma_0(p, T) + gamma_r(p, T)))
    
    End If

End If

End Function

Function v_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spezifisches Volumen von Wasserdampf in m³/kg

If p < 0 Then

    v_W = -1

Else:

    If p > ps(T) Then 'Automatisches Umschalten auf Wasser

        v_W = v_W_r1(p, T)

    Else: v_W = v_WD(p, T)
    
    End If

End If

End Function

Function v_WD(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spez. Vol. von Wasserdampf in m³/kg

R_W = 0.461526
pi_g = p / 1000

If pi_g < 0 Then 'Fehlermeldung bei unmöglicher Wasserbeladung

    v_WD = -1

Else: v_WD = pi_g * ((1 / pi_g) + gamma_r_pi(p, T)) * R_W * T / p

End If

End Function

Function rho_WD(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Dichte von Wasserdampf in kg/m³

rho_WD = 1 / v_WD(p, T)
    
End Function

Function cp_W_r1(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spezifische isobare Wärmekapazität von Wasser in kJ/kg/K

R_W = 0.461526
tau = 1386 / T

cp_W_r1 = -(tau ^ 2) * R_W * (gamma_r1_tautau(p, T))

End Function

Function h_W_r1(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spezifische Enthalpie von Wasser in kJ/kg

R_W = 0.461526
tau = 1386 / T

h_W_r1 = tau * R_W * T * (gamma_r1_tau(p, T))

End Function

Function s_W_r1(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spezifische Entropie von Wasser in kJ/kg

R_W = 0.461526
tau = 1386 / T

s_W_r1 = R_W * (tau * gamma_r1_tau(p, T) - gamma_r1(p, T))

End Function

Function v_W_r1(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: spez. Vol. von Wasser in m³/kg

R_W = 0.461526
pi_r1 = p / 16530

If T < Ts_I(p) Then

    v_W_r1 = pi_r1 * gamma_r1_pi(p, T) * R_W * T / p

Else: v_W_r1 = v_WD(p, T) 'automatisches Umschalten auf Wasserdampf

End If

End Function

Function rho_W_r1(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Dichte von Wasser in kg/m³

rho_W_r1 = 1 / v_W_r1(p, T)

End Function


Function KomZ(z_re, z_im)
KomZ = Application.WorksheetFunction.Complex(z_re, z_im)
End Function

Function KomS(z_1, z_2)
KomS = Application.WorksheetFunction.ImSum(z_1, z_2)
End Function

Function KomM(z_1, z_2)
KomM = Application.WorksheetFunction.ImProduct(z_1, z_2)
End Function

Function KomD(z_1, z_2)
KomD = Application.WorksheetFunction.ImDiv(z_1, z_2)
End Function

Function KomP(z_1, z_2)
KomP = Application.WorksheetFunction.ImPower(z_1, z_2)
End Function

Function KomR(z_com)
KomR = Application.WorksheetFunction.ImReal(z_com)
End Function

Function KomI(z_com)
KomI = Application.WorksheetFunction.Imaginary(z_com)
End Function

Function KomB(z_com)
KomB = Application.WorksheetFunction.ImAbs(z_com)
End Function

Function KomLn(z_com)
KomLn = Application.WorksheetFunction.ImLn(z_com)
End Function

Function g_Ice(p As Double, T As Double)
'Eingabe: p in kPa, T in K
'Ausgabe: spezifische Gibbs-Energie von Eis

Ice_g00 = -632020.233335886
Ice_g01 = 0.655022213658955
Ice_g02 = -1.89369929326131E-08
Ice_g03 = 3.39746123271053E-15
Ice_g04 = -5.56464869058991E-22
Ice_s0_abs = 189.13
Ice_s0_IA95 = -3327.33756492168
Ice_t1_r = 3.68017112855051E-02
Ice_t1_i = 5.10878114959572E-02
Ice_r1_r = 44.7050716285388
Ice_r1_i = 65.6876847463481
Ice_t2_r = 0.337315741065416
Ice_t2_i = 0.335449415919309
Ice_r20_r = -72.597457432922
Ice_r20_i = -78.100842711287
Ice_r21_r = -5.57107698030123E-05
Ice_r21_i = 4.64578634580806E-05
Ice_r22_r = 2.34801409215913E-11
Ice_r22_i = -2.85651142904972E-11

Ice_t1 = KomZ(Ice_t1_r, Ice_t1_i)
Ice_t2 = KomZ(Ice_t2_r, Ice_t2_i)
Ice_r1 = KomZ(Ice_r1_r, Ice_r1_i)
Ice_r20 = KomZ(Ice_r20_r, Ice_r20_i)
Ice_r21 = KomZ(Ice_r21_r, Ice_r21_i)
Ice_r22 = KomZ(Ice_r22_r, Ice_r22_i)

p_t_Ice = 0.611654771007894
p_0_Ice = 101.325
T_t_Ice = 273.16

pi_Ice = p / p_t_Ice
pi_0_Ice = p_0_Ice / p_t_Ice
tau_Ice = T / T_t_Ice

P_Ice = pi_Ice - pi_0_Ice

Ice_g0 = Ice_g00 * (P_Ice ^ 0) + Ice_g01 * (P_Ice ^ 1) + Ice_g02 * (P_Ice ^ 2) + Ice_g03 * (P_Ice ^ 3) + Ice_g04 * (P_Ice ^ 4)
Ice_r2 = KomS(KomS(KomM(Ice_r20, P_Ice ^ 0), KomM(Ice_r21, P_Ice ^ 1)), KomM(Ice_r22, P_Ice ^ 2))

ZWIce11 = KomM(KomS(Ice_t1, -tau_Ice), KomLn(KomS(Ice_t1, -tau_Ice)))
ZWIce12 = KomM(KomS(Ice_t1, tau_Ice), KomLn(KomS(Ice_t1, tau_Ice)))
ZWIce13 = KomM(KomM(-2, Ice_t1), KomLn(Ice_t1))
ZWIce14 = KomM(-1, KomD((tau_Ice ^ 2), Ice_t1))
ZWIce1 = KomM(Ice_r1, KomS(KomS(ZWIce11, ZWIce12), KomS(ZWIce13, ZWIce14)))
ZWIce21 = KomM(KomS(Ice_t2, -tau_Ice), KomLn(KomS(Ice_t2, -tau_Ice)))
ZWIce22 = KomM(KomS(Ice_t2, tau_Ice), KomLn(KomS(Ice_t2, tau_Ice)))
ZWIce23 = KomM(KomM(-2, Ice_t2), KomLn(Ice_t2))
ZWIce24 = KomM(-1, KomD((tau_Ice ^ 2), Ice_t2))
ZWIce2 = KomM(Ice_r2, KomS(KomS(ZWIce21, ZWIce22), KomS(ZWIce23, ZWIce24)))
ZWIce3 = KomR(KomS(ZWIce1, ZWIce2))

g_Ice = Ice_g0 - Ice_s0_IA95 * T_t_Ice * tau_Ice + T_t_Ice * ZWIce3

End Function

Function g_Ice_T(p As Double, T As Double)
'Eingabe: p in kPa, T in K
'Ausgabe: partielle Ableitung der spezifische Gibbs-Energie von Eis nach der Temperatur

Ice_s0_abs = 189.13
Ice_s0_IA95 = -3327.33756492168
Ice_t1_r = 3.68017112855051E-02
Ice_t1_i = 5.10878114959572E-02
Ice_r1_r = 44.7050716285388
Ice_r1_i = 65.6876847463481
Ice_t2_r = 0.337315741065416
Ice_t2_i = 0.335449415919309
Ice_r20_r = -72.597457432922
Ice_r20_i = -78.100842711287
Ice_r21_r = -5.57107698030123E-05
Ice_r21_i = 4.64578634580806E-05
Ice_r22_r = 2.34801409215913E-11
Ice_r22_i = -2.85651142904972E-11

Ice_t1 = KomZ(Ice_t1_r, Ice_t1_i)
Ice_t2 = KomZ(Ice_t2_r, Ice_t2_i)
Ice_r1 = KomZ(Ice_r1_r, Ice_r1_i)
Ice_r20 = KomZ(Ice_r20_r, Ice_r20_i)
Ice_r21 = KomZ(Ice_r21_r, Ice_r21_i)
Ice_r22 = KomZ(Ice_r22_r, Ice_r22_i)

p_t_Ice = 0.611654771007894
p_0_Ice = 101.325
T_t_Ice = 273.16
pi_Ice = p / p_t_Ice
pi_0_Ice = p_0_Ice / p_t_Ice
tau_Ice = T / T_t_Ice
P_Ice = pi_Ice - pi_0_Ice

Ice_r2 = KomS(KomS(KomM(Ice_r20, P_Ice ^ 0), KomM(Ice_r21, P_Ice ^ 1)), KomM(Ice_r22, P_Ice ^ 2))

ZWIceT1 = KomS(KomS(KomM(-1, KomLn(KomS(Ice_t1, -tau_Ice))), KomLn(KomS(Ice_t1, tau_Ice))), KomM(-2, KomD(tau_Ice, Ice_t1)))
ZWIceT2 = KomS(KomS(KomM(-1, KomLn(KomS(Ice_t2, -tau_Ice))), KomLn(KomS(Ice_t2, tau_Ice))), KomM(-2, KomD(tau_Ice, Ice_t2)))
ZWIceT3 = KomR(KomS(KomM(Ice_r1, ZWIceT1), KomM(Ice_r2, ZWIceT2)))

g_Ice_T = ZWIceT3 - Ice_s0_IA95

End Function

Function g_Ice_TT(p As Double, T As Double)
'Eingabe: p in kPa, T in K
'Ausgabe: zweite partielle Ableitung der spezifische Gibbs-Energie von Eis nach der Temperatur

Ice_t1_r = 3.68017112855051E-02
Ice_t1_i = 5.10878114959572E-02
Ice_r1_r = 44.7050716285388
Ice_r1_i = 65.6876847463481
Ice_t2_r = 0.337315741065416
Ice_t2_i = 0.335449415919309
Ice_r20_r = -72.597457432922
Ice_r20_i = -78.100842711287
Ice_r21_r = -5.57107698030123E-05
Ice_r21_i = 4.64578634580806E-05
Ice_r22_r = 2.34801409215913E-11
Ice_r22_i = -2.85651142904972E-11

Ice_t1 = KomZ(Ice_t1_r, Ice_t1_i)
Ice_t2 = KomZ(Ice_t2_r, Ice_t2_i)
Ice_r1 = KomZ(Ice_r1_r, Ice_r1_i)
Ice_r20 = KomZ(Ice_r20_r, Ice_r20_i)
Ice_r21 = KomZ(Ice_r21_r, Ice_r21_i)
Ice_r22 = KomZ(Ice_r22_r, Ice_r22_i)

p_t_Ice = 0.611654771007894
p_0_Ice = 101.325
T_t_Ice = 273.16
pi_Ice = p / p_t_Ice
pi_0_Ice = p_0_Ice / p_t_Ice
tau_Ice = T / T_t_Ice
P_Ice = pi_Ice - pi_0_Ice

Ice_r2 = KomS(KomS(KomM(Ice_r20, P_Ice ^ 0), KomM(Ice_r21, P_Ice ^ 1)), KomM(Ice_r22, P_Ice ^ 2))

ZWIceTT1 = KomS(KomS(KomD(1, KomS(Ice_t1, -tau_Ice)), KomD(1, KomS(Ice_t1, tau_Ice))), KomD(-2, Ice_t1))
ZWIceTT2 = KomS(KomS(KomD(1, KomS(Ice_t2, -tau_Ice)), KomD(1, KomS(Ice_t2, tau_Ice))), KomD(-2, Ice_t2))
ZWIceTT3 = KomR(KomS(KomM(Ice_r1, ZWIceTT1), KomM(Ice_r2, ZWIceTT2)))

g_Ice_TT = ZWIceTT3 / T_t_Ice

End Function

Function g_Ice_p(p As Double, T As Double)
'Eingabe: p in kPa, T in K
'Ausgabe: partielle Ableitung der spezifische Gibbs-Energie von Eis nach dem Druck

Ice_g01 = 0.655022213658955
Ice_g02 = -1.89369929326131E-08
Ice_g03 = 3.39746123271053E-15
Ice_g04 = -5.56464869058991E-22
Ice_s0_abs = 189.13
Ice_s0_IA95 = -3327.33756492168
Ice_t2_r = 0.337315741065416
Ice_t2_i = 0.335449415919309
Ice_r21_r = -5.57107698030123E-05
Ice_r21_i = 4.64578634580806E-05
Ice_r22_r = 2.34801409215913E-11
Ice_r22_i = -2.85651142904972E-11

Ice_t2 = KomZ(Ice_t2_r, Ice_t2_i)
Ice_r21 = KomZ(Ice_r21_r, Ice_r21_i)
Ice_r22 = KomZ(Ice_r22_r, Ice_r22_i)

p_t_Ice = 0.611654771007894
p_0_Ice = 101.325
T_t_Ice = 273.16

pi_Ice = p / p_t_Ice
pi_0_Ice = p_0_Ice / p_t_Ice
tau_Ice = T / T_t_Ice

P_Ice = pi_Ice - pi_0_Ice

Ice_g0p = Ice_g01 * (1 / (p_t_Ice * 1000)) * (P_Ice ^ (1 - 1)) + Ice_g02 * (2 / (p_t_Ice * 1000)) * (P_Ice ^ (2 - 1)) + Ice_g03 * (3 / (p_t_Ice * 1000)) * (P_Ice ^ (3 - 1)) + Ice_g04 * (4 / (p_t_Ice * 1000)) * (P_Ice ^ (4 - 1))
Ice_r2p = KomS(KomM(KomM(Ice_r21, 1 / (p_t_Ice * 1000)), P_Ice ^ (1 - 1)), KomM(KomM(Ice_r22, 1 / (p_t_Ice * 1000)), P_Ice ^ (2 - 1)))

ZWIcep1 = KomS(KomS(KomM(KomS(Ice_t2, -tau_Ice), KomLn(KomS(Ice_t2, -tau_Ice))), KomM(KomS(Ice_t2, tau_Ice), KomLn(KomS(Ice_t2, tau_Ice)))), KomS(KomM(KomM(-2, Ice_t2), KomLn(Ice_t2)), KomD(-tau_Ice ^ 2, Ice_t2)))
ZWIcep2 = KomR(KomM(Ice_r2p, ZWIcep1))

g_Ice_p = Ice_g0p + T_t_Ice * ZWIcep2

End Function

Function v_Ice(p As Double, T As Double) As Double
'Eingabe: p in kPa, t in K
'Ausgabe: spezifisches Volumen von Wasser-Eis in m³/kg

FEXL_Korr1 = -7.10935025488579E-07
FEXL_Korr2 = -2.13229864393371E-05
FEXL_Korr3 = -1.52316277916147E-04
FEXL_Korr4 = 0.994090070084641

t_Cels = T - 273.15
FEXL_Korr = FEXL_Korr1 * t_Cels ^ 3 + FEXL_Korr2 * t_Cels ^ 2 + FEXL_Korr3 * t_CelsFEXL_Korr4

v_Ice = g_Ice_p(p, T) '* FEXL_Korr

End Function

Function rho_Ice(p As Double, T As Double) As Double
'Eingabe: p in kPa, t in K
'Ausgabe: Dichte von Wasser-Eis in kg/m³

rho_Ice = 1 / g_Ice_p(p, T)

End Function

Function h_Ice(p As Double, T As Double) As Double
'Eingabe: p in kPa, t in K
'Ausgabe: spezifische Enthalpie von Wasser-Eis in kJ/kg

h_Ice = 0.001 * (g_Ice(p, T) - T * g_Ice_T(p, T))

End Function

Function u_Ice(p As Double, T As Double) As Double
'Eingabe: p in kPa, t in K
'Ausgabe: spezifische innnere Energie von Wasser-Eis in kJ/kg

u_Ice = 0.001 * (g_Ice(p, T) - T * g_Ice_T(p, T) - p * 1000 * g_Ice_p(p, T))

End Function

Function s_Ice(p As Double, T As Double) As Double
'Eingabe: p in kPa, t in K
'Ausgabe: spezifische Entropie von Wasser-Eis in kJ/kg/K

s_Ice = -0.001 * g_Ice_T(p, T)

End Function

Function cp_Ice(p As Double, T As Double) As Double
'Eingabe: p in kPa, t in K
'Ausgabe: spezifische isobare Wärmekapazität von Wasser-Eis in kJ/kg/K

cp_Ice = -0.001 * T * g_Ice_TT(p, T)

End Function
Function eta_0_W(T As Double) As Double
'Eingabe: T in K
'Ausgabe: Idealgasanteil der dynamischen Viskosität

eta_H0 = 1.67752
eta_H1 = 2.20462
eta_H2 = 0.6366564
eta_H3 = -0.241605

T_quer = T / 647.096

eta_0_Nen = eta_H0 / (T_quer ^ 0) + eta_H1 / (T_quer ^ 1) + eta_H2 / (T_quer ^ 2) + eta_H3 / (T_quer ^ 3)

eta_0_W = 100 * Sqr(T_quer) / eta_0_Nen

End Function

Function eta_1_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Residualanteil der dynamischen Viskosität

eta_H00 = 0.520094
eta_H01 = 0.222531
eta_H02 = -0.281378
eta_H03 = 0.161913
eta_H04 = -0.0325372
eta_H10 = 0.0850895
eta_H11 = 0.999115
eta_H12 = -0.906851
eta_H13 = 0.257399
eta_H20 = -1.08374
eta_H21 = 1.88797
eta_H22 = -0.772479
eta_H30 = -0.289555
eta_H31 = 1.26613
eta_H32 = -0.489837
eta_H34 = 0.0698452
eta_H36 = -0.00435673
eta_H42 = -0.25704
eta_H45 = 0.00872102
eta_H51 = 0.120573
eta_H56 = -0.000593264

T_quer = T / 647.096

If p <= ps(T) Then
    
    rho_quer = rho_WD(p, T) / 322

Else: rho_quer = rho_W_r1(p, T) / 322

End If

T_K = (1 / T_quer) - 1
rho_K = rho_quer - 1

eta_1_S0 = eta_H00 * (rho_K ^ 0) + eta_H01 * (rho_K ^ 1) + eta_H02 * (rho_K ^ 2) + eta_H03 * (rho_K ^ 3) + eta_H04 * (rho_K ^ 4)
eta_1_S1 = eta_H10 * (rho_K ^ 0) + eta_H11 * (rho_K ^ 1) + eta_H12 * (rho_K ^ 2) + eta_H13 * (rho_K ^ 3)
eta_1_S2 = eta_H20 * (rho_K ^ 0) + eta_H21 * (rho_K ^ 1) + eta_H22 * (rho_K ^ 2)
eta_1_S3 = eta_H30 * (rho_K ^ 0) + eta_H31 * (rho_K ^ 1) + eta_H32 * (rho_K ^ 2) + eta_H34 * (rho_K ^ 4) + eta_H36 * (rho_K ^ 6)
eta_1_S4 = eta_H42 * (rho_K ^ 2) + eta_H45 * (rho_K ^ 5)
eta_1_S5 = eta_H51 * (rho_K ^ 1) + eta_H56 * (rho_K ^ 6)

eta_1_W = Exp(rho_quer * ((T_K ^ 0) * eta_1_S0 + (T_K ^ 1) * eta_1_S1 + (T_K ^ 2) * eta_1_S2 + (T_K ^ 3) * eta_1_S3 + (T_K ^ 4) * eta_1_S4 + (T_K ^ 5) * eta_1_S5))

End Function

Function eta_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: dynamische Viskosität in Pa*s

If T < 273.15 And p > ps(T) Then

    eta_W = -1 'Berechnung für Eiskristalle Ih nicht möglich

Else: eta_W = eta_0_W(T) * eta_1_W(p, T) * 0.000001

End If

End Function

Function lambda_W_0(T As Double) As Double
'Eingabe: T in K
'Ausgabe: dilute-gas contribution to thermal conductivity in W/m/K

lai_a0 = 0.0102811
lai_a1 = 0.0299621
lai_a2 = 0.0156146
lai_a3 = -0.00422464

T_stern_ind = 647.26
T_r_i = T / T_stern_ind

lambda_W_0 = Sqr(T_r_i) * (lai_a0 * (T_r_i ^ 0) + lai_a1 * (T_r_i ^ 1) + lai_a2 * (T_r_i ^ 2) + lai_a3 * (T_r_i ^ 3))

End Function

Function lambda_W_1(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: residual part contribution to thermal conductivity in W/m/K

lai_b0 = -0.39707
lai_b1 = 0.400302
lai_b2 = 1.06
lai_bb1 = -0.171587
lai_bb2 = 2.39219

T_stern_ind = 647.26
rho_stern_ind = 317.7
T_r_i = T / T_stern_ind

If p <= ps(T) Then

    rho_r_i = rho_WD(p, T) / rho_stern_ind
    
Else: rho_r_i = rho_W_r1(p, T) / rho_stern_ind

End If

lambda_W_1 = lai_b0 + lai_b1 * rho_r_i + lai_b2 * Exp(lai_bb1 * (rho_r_i + lai_bb2) ^ 2)

End Function

Function lambda_W_2(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: critical enhancement contribution to thermal conductivity in W/m/K

lai_d1 = 0.0701309
lai_d2 = 0.011852
lai_d3 = 0.00169937
lai_d4 = -1.02
lai_c1 = 0.642857
lai_c2 = -4.11717
lai_c3 = -6.17937
lai_c4 = 0.00308976
lai_c5 = 0.0822994
lai_c6 = 10.0932

T_stern_ind = 647.26
rho_stern_ind = 317.7
T_r_i = T / T_stern_ind

If p <= ps(T) Then

    rho_r_i = rho_WD(p, T) / rho_stern_ind
    
Else: rho_r_i = rho_W_r1(p, T) / rho_stern_ind

End If

del_T_r = Abs(T_r_i - 1) + lai_c4

lai_q = 2 + lai_c5 / (del_T_r ^ 0.6)

If del_T_r >= 1 Then

    lai_s = 1 / del_T_r
    
Else: lai_s = lai_c6 / (del_T_r ^ 0.6)

End If

lambda_W_ind2_S1 = ((lai_d1 / (T_r_i ^ 10)) + lai_d2) * (rho_r_i ^ 1.8) * Exp(lai_c1 * (1 - rho_r_i ^ 2.8))
lambda_W_ind2_S2 = lai_d3 * lai_s * (rho_r_i ^ lai_q) * Exp((lai_q / (1 + lai_q)) * (1 - rho_r_i ^ (1 + lai_q)))
lambda_W_ind2_S3 = lai_d4 * Exp(lai_c2 * (T_r_i ^ 1.5) + lai_c3 / (rho_r_i ^ 5))

lambda_W_2 = lambda_W_ind2_S1 + lambda_W_ind2_S2 + lambda_W_ind2_S3

End Function

Function lambda_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: thermal conductivity in W/m/K

If T < 273.16 And p > ps(T) Then

    lambda_W = 2.21

Else: lambda_W = lambda_W_0(T) + lambda_W_1(p, T) + lambda_W_2(p, T)

End If

End Function

Function ny_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: kinematische Viskosität in m²/s

If p <= ps(T) Then

    ny_W = eta_W(p, T) / rho_WD(p, T)
    
Else: ny_W = eta_W(p, T) / rho_W_r1(p, T)

End If

End Function

Function a_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Temperaturleitfähigkeit in m²/s

If p <= ps(T) Then

    a_W = lambda_W(p, T) / cp_W(p, T) / rho_WD(p, T)
    
Else: a_W = lambda_W(p, T) / cp_W(p, T) / rho_W_r1(p, T)

End Function

Function Pr_W(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Prandtl-Zahl

Pr_W = 1000 * ny_W(p, T) / a_W(p, T)

End Function

Function rho_Lemm(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Dichte trockener Luft in kg/m³

rho_Lemm = p / T / 0.287117 / Z_trL(p, T)

End Function

Function rho_Lemm_mol(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Dichte trockener Luft in mol/dm³

rho_Lemm_mol = p / T / 8.31451

End Function

Function cp_trL_0(T As Double) As Double
'Eingabe: T in K
'Ausgabe: spezifische isobare Wärmekapazität von trockener Luft als Idealgas c_p,L in kJ/kg*K

R_Lemmon = 8.31451
M_Lemmon = 28.9586
Lemm_N1 = 3.490888032
Lemm_N2 = 0.000002395525583
Lemm_N3 = 7.172111248E-09
Lemm_N4 = -3.115413101E-13
Lemm_N5 = 0.223806688
Lemm_N6 = 0.791309509
Lemm_N7 = 0.212236768
Lemm_N8 = 0.197938904
Lemm_N9 = 3364.011
Lemm_N10 = 2242.45
Lemm_N11 = 11580.4

Lemm_u = Lemm_N9 / T
Lemm_v = Lemm_N10 / T
Lemm_w = Lemm_N11 / T

sum1 = (Lemm_N1 * 1) + (Lemm_N2 * T) + (Lemm_N3 * T ^ 2) + (Lemm_N4 * T ^ 3)
sum2 = Lemm_N5 / (T ^ 1.5)
sum3 = Lemm_N6 * (Lemm_u ^ 2) * Exp(Lemm_u) / ((Exp(Lemm_u) - 1) ^ 2)
sum4 = Lemm_N7 * (Lemm_v ^ 2) * Exp(Lemm_v) / ((Exp(Lemm_v) - 1) ^ 2)
sum5 = (2 / 3) * Lemm_N8 * (Lemm_w ^ 2) * Exp(-Lemm_w) / (((2 / 3) * Exp(-Lemm_w) + 1) ^ 2)

cp0 = R_Lemmon * (sum1 + sum2 + sum3 + sum4 + sum5)
cp_trL_0 = cp0 / M_Lemmon

End Function

Function alpha_0(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Idealgasanteil der dimensionslosen Helmholtzenergie alpha_0

L_a1 = 0.00000006057194
L_a2 = -0.0000210274769
L_a3 = -0.000158860716
L_a4 = -13.841928076
L_a5 = 17.275266575
L_a6 = -0.00019536342
L_a7 = 2.490888032
L_a8 = 0.791309509
L_a9 = 0.212236768
L_a10 = -0.197938904
L_a11 = 25.36365
L_a12 = 16.90741
L_a13 = 87.31279

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

a01 = Log(del)
a02 = L_a1 * tau ^ (-3) + L_a2 * tau ^ (-2) + L_a3 * tau ^ (-1) + L_a4 + L_a5 * tau
a03 = L_a6 * tau ^ 1.5
a04 = L_a7 * Log(tau)
a05 = L_a8 * Log(1 - Exp((-L_a11) * tau))
a06 = L_a9 * Log(1 - Exp((-L_a12) * tau))
a07 = L_a10 * Log((2 / 3) + Exp(L_a13 * tau))

alpha_0 = a01 + a02 + a03 + a04 + a05 + a06 + a07

End Function

Function alpha_r(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Residualanteil der dimensionslosen Helmholtzenergie alpha_r

L_ar1 = 0.118160747229
L_ar2 = 0.713116392079
L_ar3 = -1.61824192067
L_ar4 = 0.0714140178971
L_ar5 = -0.0865421396646
L_ar6 = 0.134211176704
L_ar7 = 0.0112626704218
L_ar8 = -0.0420533228842
L_ar9 = 0.0349008431982
L_ar10 = 0.000164957183186
L_ar11 = -0.101365037912
L_ar12 = -0.17381369097
L_ar13 = -0.0472103183731
L_ar14 = -0.0122523554253
L_ar15 = -0.146629609713
L_ar16 = -0.0316055879821
L_ar17 = 0.000233594806142
L_ar18 = 0.0148287891978
L_ar19 = -0.00938782884667

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

ar1 = L_ar1 * del ^ 1 * tau ^ 0 + L_ar2 * del ^ 1 * tau ^ 0.33 + L_ar3 * del ^ 1 * tau ^ 1.01 + L_ar4 * del ^ 2 * tau ^ 0 + L_ar5 * del ^ 3 * tau ^ 0 + L_ar6 * del ^ 3 * tau ^ 0.15 + L_ar7 * del ^ 4 * tau ^ 0 + L_ar8 * del ^ 4 * tau ^ 0.2 + L_ar9 * del ^ 4 * tau ^ 0.35 + L_ar10 * del ^ 5 * tau ^ 1.35
ar2 = L_ar11 * del ^ 1 * tau ^ 1.6 * Exp(-del ^ 1) + L_ar12 * del ^ 3 * tau ^ 0.8 * Exp(-del ^ 1) + L_ar13 * del ^ 5 * tau ^ 0.95 * Exp(-del ^ 1) + L_ar14 * del ^ 6 * tau ^ 1.25 * Exp(-del ^ 1) + L_ar15 * del ^ 1 * tau ^ 3.6 * Exp(-del ^ 2) + L_ar16 * del ^ 3 * tau ^ 6 * Exp(-del ^ 2) + L_ar17 * del ^ 11 * tau ^ 3.25 * Exp(-del ^ 2) + L_ar18 * del ^ 1 * tau ^ 3.5 * Exp(-del ^ 3) + L_ar19 * del ^ 3 * tau ^ 15 * Exp(-del ^ 3)

alpha_r = ar1 + ar2

End Function

Function alpha_ges(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: dimensionslose Helmholtzenergie nach Lemmon

alpha_ges = alpha_0(p, T) + alpha_r(p, T)

End Function

Function alpha_0_t(T As Double) As Double
'Eingabe: T in K
'Ausgabe: zweite Ableitung der Idealgas-Helmholtzenergie nach der red. Temp.

L_a1 = 0.00000006057194
L_a2 = -0.0000210274769
L_a3 = -0.000158860716
L_a4 = -13.841928076
L_a5 = 17.275266575
L_a6 = -0.00019536342
L_a7 = 2.490888032
L_a8 = 0.791309509
L_a9 = 0.212236768
L_a10 = -0.197938904
L_a11 = 25.36365
L_a12 = 16.90741
L_a13 = 87.31279

T_j = 132.6312
tau = T_j / T

a_0_t_1 = (1 - 4) * L_a1 * tau ^ (1 - 5) + (2 - 4) * L_a2 * tau ^ (2 - 5) + (3 - 4) * L_a3 * tau ^ (3 - 5) + (4 - 4) * L_a4 * tau ^ (4 - 5) + (5 - 4) * L_a5 * tau ^ (5 - 5)
a_0_t_2 = 1.5 * L_a6 * tau ^ 0.5
a_0_t_3 = L_a7 / tau
a_0_t_4 = L_a8 * L_a11 / (Exp(L_a11 * tau) - 1)
a_0_t_5 = L_a9 * L_a12 / (Exp(L_a12 * tau) - 1)
a_0_t_6 = L_a10 * L_a13 / ((2 / 3) * Exp(-L_a13 * tau) + 1)

alpha_0_t = a_0_t_1 + a_0_t_2 + a_0_t_3 + a_0_t_4 + a_0_t_5 + a_0_t_6
 
End Function

Function alpha_0_tt(T As Double) As Double
'Eingabe: T in K
'Ausgabe: zweite Ableitung der Idealgas-Helmholtzenergie nach der red. Temp.

L_a1 = 0.00000006057194
L_a2 = -0.0000210274769
L_a3 = -0.000158860716
L_a4 = -13.841928076
L_a5 = 17.275266575
L_a6 = -0.00019536342
L_a7 = 2.490888032
L_a8 = 0.791309509
L_a9 = 0.212236768
L_a10 = -0.197938904
L_a11 = 25.36365
L_a12 = 16.90741
L_a13 = 87.31279

T_j = 132.6312
tau = T_j / T

a_0_tt_1 = (1 - 4) * (1 - 5) * L_a1 * tau ^ (1 - 6) + (2 - 4) * (2 - 5) * L_a2 * tau ^ (2 - 6) + (3 - 4) * (3 - 5) * L_a3 * tau ^ (3 - 6) + (4 - 4) * (4 - 5) * L_a4 * tau ^ (4 - 6) + (5 - 4) * (5 - 5) * L_a5 * tau ^ (5 - 6)
a_0_tt_2 = 0.75 * L_a6 * tau ^ -0.5
a_0_tt_3 = -L_a7 / (tau ^ 2)
a_0_tt_4 = -L_a8 * L_a11 * L_a11 * Exp(tau * L_a11) / ((Exp(L_a11 * tau) - 1) ^ 2)
a_0_tt_5 = -L_a9 * L_a12 * L_a12 * Exp(tau * L_a12) / ((Exp(L_a12 * tau) - 1) ^ 2)
a_0_tt_6 = (2 / 3) * L_a10 * L_a13 * L_a13 * Exp(-L_a13 * tau) / (((2 / 3) * Exp(-L_a13 * tau) + 1) ^ 2)

alpha_0_tt = a_0_tt_1 + a_0_tt_2 + a_0_tt_3 + a_0_tt_4 + a_0_tt_5 + a_0_tt_6
 
End Function

Function alpha_r_d(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: erste Ableitung der Residual-Helmholtzenergie nach der red. Dichte

L_ar1 = 0.118160747229
L_ar2 = 0.713116392079
L_ar3 = -1.61824192067
L_ar4 = 0.0714140178971
L_ar5 = -0.0865421396646
L_ar6 = 0.134211176704
L_ar7 = 0.0112626704218
L_ar8 = -0.0420533228842
L_ar9 = 0.0349008431982
L_ar10 = 0.000164957183186
L_ar11 = -0.101365037912
L_ar12 = -0.17381369097
L_ar13 = -0.0472103183731
L_ar14 = -0.0122523554253
L_ar15 = -0.146629609713
L_ar16 = -0.0316055879821
L_ar17 = 0.000233594806142
L_ar18 = 0.0148287891978
L_ar19 = -0.00938782884667

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

a_r_d_1 = 1 * L_ar1 * (del ^ (1 - 1)) * (tau ^ 0) + 1 * L_ar2 * (del ^ (1 - 1)) * (tau ^ 0.33) + 1 * L_ar3 * (del ^ (1 - 1)) * (tau ^ 1.01) + 2 * L_ar4 * (del ^ (2 - 1)) * (tau ^ 0) + 3 * L_ar5 * (del ^ (3 - 1)) * (tau ^ 0) + 3 * L_ar6 * (del ^ (3 - 1)) * (tau ^ 0.15) + 4 * L_ar7 * (del ^ (4 - 1)) * (tau ^ 0) + 4 * L_ar8 * (del ^ (4 - 1)) * (tau ^ 0.2) + 4 * L_ar9 * (del ^ (4 - 1)) * (tau ^ 0.35) + 5 * L_ar10 * (del ^ (5 - 1)) * (tau ^ 0.35)
a_r_d_2 = L_ar11 * (del ^ (1 - 1)) * (tau ^ 1.6) * Exp(-del ^ 1) * (1 - 1 * del ^ 1) + L_ar12 * (del ^ (3 - 1)) * (tau ^ 0.8) * Exp(-del ^ 1) * (3 - 1 * del ^ 1) + L_ar13 * (del ^ (5 - 1)) * (tau ^ 0.95) * Exp(-del ^ 1) * (5 - 1 * del ^ 1) + L_ar14 * (del ^ (6 - 1)) * (tau ^ 1.25) * Exp(-del ^ 1) * (6 - 1 * del ^ 1) + L_ar15 * (del ^ (1 - 1)) * (tau ^ 3.6) * Exp(-del ^ 2) * (1 - 2 * del ^ 2) + L_ar16 * (del ^ (3 - 1)) * (tau ^ 6) * Exp(-del ^ 2) * (3 - 2 * del ^ 2) + L_ar17 * (del ^ (11 - 1)) * (tau ^ 3.25) * Exp(-del ^ 2) * (11 - 2 * del ^ 2) + L_ar18 * (del ^ (1 - 1)) * (tau ^ 3.5) * Exp(-del ^ 3) * (1 - 3 * del ^ 3) + L_ar19 * (del ^ (3 - 1)) * (tau ^ 15) * Exp(-del ^ 3) * (3 - 3 * del ^ 3)

alpha_r_d = a_r_d_1 + a_r_d_2

End Function

Function alpha_r_dd(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: zweite Ableitung der Residual-Helmholtzenergie nach der red. Dichte

L_ar1 = 0.118160747229
L_ar2 = 0.713116392079
L_ar3 = -1.61824192067
L_ar4 = 0.0714140178971
L_ar5 = -0.0865421396646
L_ar6 = 0.134211176704
L_ar7 = 0.0112626704218
L_ar8 = -0.0420533228842
L_ar9 = 0.0349008431982
L_ar10 = 0.000164957183186
L_ar11 = -0.101365037912
L_ar12 = -0.17381369097
L_ar13 = -0.0472103183731
L_ar14 = -0.0122523554253
L_ar15 = -0.146629609713
L_ar16 = -0.0316055879821
L_ar17 = 0.000233594806142
L_ar18 = 0.0148287891978
L_ar19 = -0.00938782884667

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

a_r_dd_1 = 1 * (1 - 1) * L_ar1 * (del ^ (1 - 2)) * (tau ^ 0) + 1 * (1 - 1) * L_ar2 * (del ^ (1 - 2)) * (tau ^ 0.33) + 1 * (1 - 1) * L_ar3 * (del ^ (1 - 2)) * (tau ^ 1.01) + 2 * (2 - 1) * L_ar4 * (del ^ (2 - 2)) * (tau ^ 0) + 3 * (3 - 1) * L_ar5 * (del ^ (3 - 2)) * (tau ^ 0) + 3 * (3 - 1) * L_ar6 * (del ^ (3 - 2)) * (tau ^ 0.15) + 4 * (4 - 1) * L_ar7 * (del ^ (4 - 2)) * (tau ^ 0) + 4 * (4 - 1) * L_ar8 * (del ^ (4 - 2)) * (tau ^ 0.2) + 4 * (4 - 1) * L_ar9 * (del ^ (4 - 2)) * (tau ^ 0.35) + 5 * (5 - 1) * L_ar10 * (del ^ (5 - 2)) * (tau ^ 0.35)
a_r_dd_2 = L_ar11 * (del ^ (1 - 2)) * (tau ^ 1.6) * Exp(-del ^ 1) * ((1 - 1 * del ^ 1) * (1 - 1 - 1 * del ^ 1) - 1 * 1 * del ^ 1) + L_ar12 * (del ^ (3 - 2)) * (tau ^ 0.8) * Exp(-del ^ 1) * ((3 - 1 * del ^ 1) * (3 - 1 - 1 * del ^ 1) - 1 * 1 * del ^ 1) + L_ar13 * (del ^ (5 - 2)) * (tau ^ 0.95) * Exp(-del ^ 1) * ((5 - 1 * del ^ 1) * (5 - 1 - 1 * del ^ 1) - 1 * 1 * del ^ 1) + L_ar14 * (del ^ (6 - 2)) * (tau ^ 1.25) * Exp(-del ^ 1) * ((6 - 1 * del ^ 1) * (6 - 1 - 1 * del ^ 1) - 1 * 1 * del ^ 1) + L_ar15 * (del ^ (1 - 2)) * (tau ^ 3.6) * Exp(-del ^ 2) * ((1 - 2 * del ^ 2) * (1 - 1 - 2 * del ^ 2) - 2 * 2 * del ^ 2) + L_ar16 * (del ^ (3 - 2)) * (tau ^ 6) * Exp(-del ^ 2) * ((3 - 2 * del ^ 2) * (3 - 1 - 2 * del ^ 2) - 2 * 2 * del ^ 2) + L_ar17 * (del ^ (11 - 2)) * (tau ^ 3.25) * Exp(-del ^ 2) * ((11 - 2 * del ^ 2) * (11 - 1 - 2 * del ^ 2) - 2 * 2 * del ^ 2) + L_ar18 * (del ^ (1 - 2)) * (tau ^ 3.5) * Exp(-del ^ 3) * ((1 - 3 * del ^ 3) * (1 - 1 - 3 * del ^ 3) - 3 * 3 * del ^ 3)
a_r_dd_2a = L_ar19 * (del ^ (3 - 2)) * (tau ^ 15) * Exp(-del ^ 3) * ((3 - 3 * del ^ 3) * (3 - 1 - 3 * del ^ 3) - 3 * 3 * del ^ 3)

alpha_r_dd = a_r_dd_1 + a_r_dd_2 + a_r_dd_2a

End Function

Function alpha_r_t(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: zweite Ableitung der Residual-Helmholtzenergie nach der red. Temp.

L_ar1 = 0.118160747229
L_ar2 = 0.713116392079
L_ar3 = -1.61824192067
L_ar4 = 0.0714140178971
L_ar5 = -0.0865421396646
L_ar6 = 0.134211176704
L_ar7 = 0.0112626704218
L_ar8 = -0.0420533228842
L_ar9 = 0.0349008431982
L_ar10 = 0.000164957183186
L_ar11 = -0.101365037912
L_ar12 = -0.17381369097
L_ar13 = -0.0472103183731
L_ar14 = -0.0122523554253
L_ar15 = -0.146629609713
L_ar16 = -0.0316055879821
L_ar17 = 0.000233594806142
L_ar18 = 0.0148287891978
L_ar19 = -0.00938782884667

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

a_r_t_1 = 0 * L_ar1 * (del ^ 1) * (tau ^ (0 - 1)) + 0.33 * L_ar2 * (del ^ 1) * (tau ^ (0.33 - 1)) + 1.01 * L_ar3 * (del ^ 1) * (tau ^ (1.01 - 1)) + 0 * L_ar4 * (del ^ 2) * (tau ^ (0 - 1)) + 0 * L_ar5 * (del ^ 3) * (tau ^ (0 - 1)) + 0.15 * L_ar6 * (del ^ 3) * (tau ^ (0.15 - 1)) + 0 * L_ar7 * (del ^ 4) * (tau ^ (0 - 1)) + 0.2 * L_ar8 * (del ^ 4) * (tau ^ (0.2 - 1)) + 0.35 * L_ar9 * (del ^ 4) * (tau ^ (0.35 - 1)) + 1.35 * L_ar10 * (del ^ 5) * (tau ^ (1.35 - 1))
a_r_t_2 = 1.6 * L_ar11 * (del ^ 1) * (tau ^ (1.6 - 1)) * Exp(-del ^ 1) + 0.8 * L_ar12 * (del ^ 3) * (tau ^ (0.8 - 1)) * Exp(-del ^ 1) + 0.95 * L_ar13 * (del ^ 5) * (tau ^ (0.95 - 1)) * Exp(-del ^ 1) + 1.25 * L_ar14 * (del ^ 6) * (tau ^ (1.25 - 1)) * Exp(-del ^ 1) + 3.6 * L_ar15 * (del ^ 1) * (tau ^ (3.6 - 1)) * Exp(-del ^ 2) + 6 * L_ar16 * (del ^ 3) * (tau ^ (6 - 1)) * Exp(-del ^ 2) + 3.25 * L_ar17 * (del ^ 11) * (tau ^ (3.25 - 1)) * Exp(-del ^ 2) + 3.5 * L_ar18 * (del ^ 1) * (tau ^ (3.5 - 1)) * Exp(-del ^ 3) + 15 * L_ar19 * (del ^ 3) * (tau ^ (15 - 1)) * Exp(-del ^ 3)

alpha_r_t = a_r_t_1 + a_r_t_2

End Function

Function alpha_r_tt(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: zweite Ableitung der Residual-Helmholtzenergie nach der red. Temp.

L_ar1 = 0.118160747229
L_ar2 = 0.713116392079
L_ar3 = -1.61824192067
L_ar4 = 0.0714140178971
L_ar5 = -0.0865421396646
L_ar6 = 0.134211176704
L_ar7 = 0.0112626704218
L_ar8 = -0.0420533228842
L_ar9 = 0.0349008431982
L_ar10 = 0.000164957183186
L_ar11 = -0.101365037912
L_ar12 = -0.17381369097
L_ar13 = -0.0472103183731
L_ar14 = -0.0122523554253
L_ar15 = -0.146629609713
L_ar16 = -0.0316055879821
L_ar17 = 0.000233594806142
L_ar18 = 0.0148287891978
L_ar19 = -0.00938782884667

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

a_r_tt_1 = 0 * (0 - 1) * L_ar1 * (del ^ 1) * (tau ^ (0 - 2)) + 0.33 * (0.33 - 1) * L_ar2 * (del ^ 1) * (tau ^ (0.33 - 2)) + 1.01 * (1.01 - 1) * L_ar3 * (del ^ 1) * (tau ^ (1.01 - 2)) + 0 * (0 - 1) * L_ar4 * (del ^ 2) * (tau ^ (0 - 2)) + 0 * (0 - 1) * L_ar5 * (del ^ 3) * (tau ^ (0 - 2)) + 0.15 * (0.15 - 1) * L_ar6 * (del ^ 3) * (tau ^ (0.15 - 2)) + 0 * (0 - 1) * L_ar7 * (del ^ 4) * (tau ^ (0 - 2)) + 0.2 * (0.2 - 1) * L_ar8 * (del ^ 4) * (tau ^ (0.2 - 2)) + 0.35 * (0.35 - 1) * L_ar9 * (del ^ 4) * (tau ^ (0.35 - 2)) + 1.35 * (1.35 - 1) * L_ar10 * (del ^ 5) * (tau ^ (1.35 - 2))
a_r_tt_2 = 1.6 * (1.6 - 1) * L_ar11 * (del ^ 1) * (tau ^ (1.6 - 2)) * Exp(-del ^ 1) + 0.8 * (0.8 - 1) * L_ar12 * (del ^ 3) * (tau ^ (0.8 - 2)) * Exp(-del ^ 1) + 0.95 * (0.95 - 1) * L_ar13 * (del ^ 5) * (tau ^ (0.95 - 2)) * Exp(-del ^ 1) + 1.25 * (1.25 - 1) * L_ar14 * (del ^ 6) * (tau ^ (1.25 - 2)) * Exp(-del ^ 1) + 3.6 * (3.6 - 1) * L_ar15 * (del ^ 1) * (tau ^ (3.6 - 2)) * Exp(-del ^ 2) + 6 * (6 - 1) * L_ar16 * (del ^ 3) * (tau ^ (6 - 2)) * Exp(-del ^ 2) + 3.25 * (3.25 - 1) * L_ar17 * (del ^ 11) * (tau ^ (3.25 - 2)) * Exp(-del ^ 2) + 3.5 * (3.5 - 1) * L_ar18 * (del ^ 1) * (tau ^ (3.5 - 2)) * Exp(-del ^ 3) + 15 * (15 - 1) * L_ar19 * (del ^ 3) * (tau ^ (15 - 2)) * Exp(-del ^ 3)

alpha_r_tt = a_r_tt_1 + a_r_tt_2

End Function

Function alpha_r_td(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: zweite Ableitung der Residual-Helmholtzenergie nach der red. Temp. und red. Dichte

L_ar1 = 0.118160747229
L_ar2 = 0.713116392079
L_ar3 = -1.61824192067
L_ar4 = 0.0714140178971
L_ar5 = -0.0865421396646
L_ar6 = 0.134211176704
L_ar7 = 0.0112626704218
L_ar8 = -0.0420533228842
L_ar9 = 0.0349008431982
L_ar10 = 0.000164957183186
L_ar11 = -0.101365037912
L_ar12 = -0.17381369097
L_ar13 = -0.0472103183731
L_ar14 = -0.0122523554253
L_ar15 = -0.146629609713
L_ar16 = -0.0316055879821
L_ar17 = 0.000233594806142
L_ar18 = 0.0148287891978
L_ar19 = -0.00938782884667

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

a_r_td_1 = 0 * 1 * L_ar1 * (del ^ 1) * (tau ^ (0 - 1)) + 0.33 * 1 * L_ar2 * (del ^ 1) * (tau ^ (0.33 - 1)) + 1.01 * 1 * L_ar3 * (del ^ 1) * (tau ^ (1.01 - 1)) + 0 * 2 * L_ar4 * (del ^ 2) * (tau ^ (0 - 1)) + 0 * 3 * L_ar5 * (del ^ 3) * (tau ^ (0 - 1)) + 0.15 * 3 * L_ar6 * (del ^ 3) * (tau ^ (0.15 - 1)) + 0 * 4 * L_ar7 * (del ^ 4) * (tau ^ (0 - 1)) + 0.2 * 4 * L_ar8 * (del ^ 4) * (tau ^ (0.2 - 1)) + 0.35 * 4 * L_ar9 * (del ^ 4) * (tau ^ (0.35 - 1)) + 1.35 * 5 * L_ar10 * (del ^ 5) * (tau ^ (1.35 - 1))
a_r_td_2 = 1.6 * L_ar11 * (del ^ (1 - 1)) * (tau ^ (1.6 - 1)) * Exp(-del ^ 1) * (1 - 1 * del ^ 1) + 0.8 * L_ar12 * (del ^ (3 - 1)) * (tau ^ (0.8 - 1)) * Exp(-del ^ 1) * (3 - 1 * del ^ 1) + 0.95 * L_ar13 * (del ^ (5 - 1)) * (tau ^ (0.95 - 1)) * Exp(-del ^ 1) * (5 - 1 * del ^ 1) + 1.25 * L_ar14 * (del ^ (6 - 1)) * (tau ^ (1.25 - 1)) * Exp(-del ^ 1) * (6 - 1 * del ^ 1) + 3.6 * L_ar15 * (del ^ (1 - 1)) * (tau ^ (3.6 - 1)) * Exp(-del ^ 2) * (1 - 2 * del ^ 2) + 6 * L_ar16 * (del ^ (3 - 1)) * (tau ^ (6 - 1)) * Exp(-del ^ 2) * (3 - 2 * del ^ 2) + 3.25 * L_ar17 * (del ^ (11 - 1)) * (tau ^ (3.25 - 1)) * Exp(-del ^ 2) * (11 - 2 * del ^ 2) + 3.5 * L_ar18 * (del ^ (1 - 1)) * (tau ^ (3.5 - 1)) * Exp(-del ^ 3) * (1 - 3 * del ^ 3) + 15 * L_ar19 * (del ^ (3 - 1)) * (tau ^ (15 - 1)) * Exp(-del ^ 3) * (3 - 3 * del ^ 3)

alpha_r_td = a_r_td_1 + a_r_td_2

End Function

Function Z_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Luft-Realgasfaktor Z

rho_j = 10.4477
del = rho_Lemm_mol(p, T) / rho_j

Z_trL = 1 + del * alpha_r_d(p, T)

End Function

Function h_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Enthalpie h in kJ/kg, Nullpunkt bei 273,15 K

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

h_trL = ((tau * (alpha_0_t(T) + alpha_r_t(p, T)) + del * alpha_r_d(p, T) + 1) * T * 8.31451 / 28.9586) - 273.291845966283

End Function

Function s_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Entropie s in kJ/kg/K, Nullwert 0.161802887 bei 273,15 K (nur irreversibler Anteil der Trockenluftentropie bei 273,15 K)

T_j = 132.6312

tau = T_j / T

s_trL = ((tau * (alpha_0_t(T) + alpha_r_t(p, T)) - alpha_0(p, T) - alpha_r(p, T)) * 8.31451 / 28.9586) - 6.61041543962396

End Function

Function cv_Lemm_R(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: molare isochore Wärmekapazität trockener Luft nach Lemmon, bezogen auf R

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

cv_Lemm_R = (-tau ^ 2) * (alpha_0_tt(T) + alpha_r_tt(p, T))

End Function

Function cp_Lemm_R(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: molare isobare Wärmekapazität trockener Luft nach Lemmon, bezogen auf R

T_j = 132.6312
rho_j = 10.4477

tau = T_j / T
del = rho_Lemm_mol(p, T) / rho_j

cp_Lemm_R = cv_Lemm_R(p, T) + ((1 + del * alpha_r_d(p, T) - del * tau * alpha_r_td(p, T)) / (1 + 2 * del * alpha_r_d(p, T) + del * del * alpha_r_dd(p, T)))

End Function

Function cv_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: isochore Wärmekapazität trockener Luft nach Lemmon in kJ*kg^-1*K^-1

R_Lemmon = 8.31451
M_Lemmon = 28.9586

cv_trL = cv_Lemm_R(p, T) * R_Lemmon / M_Lemmon

End Function

Function cp_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: isobare Wärmekapazität trockener Luft nach Lemmon in kJ*kg^-1*K^-1

R_Lemmon = 8.31451
M_Lemmon = 28.9586

cp_trL = cp_Lemm_R(p, T) * R_Lemmon / M_Lemmon

End Function

Function eta_0_Lemm(T As Double) As Double
'Eingabe: Temperatur T in K
'Ausgabe: Idealgasanteil der dynamischen Viskosität in Pa*s

Len_Jon_Param = 103.3
T_Stern = T / Len_Jon_Param

Sigma_T_Stern = Exp(0.431 * ((Log(T_Stern)) ^ 0) + (-0.4623) * ((Log(T_Stern)) ^ 1) + 0.08406 * ((Log(T_Stern)) ^ 2) + 0.005341 * ((Log(T_Stern)) ^ 3) + (-0.00331) * ((Log(T_Stern)) ^ 4))

eta_0_Lemm = 0.000001 * 0.0266958 * Sqr(28.9586 * T) / Sigma_T_Stern / (0.36 ^ 2)

End Function

Function eta_r_Lemm(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Residualanteil der dynamischen Viskosität in Pa*s

tau_eta_r = 132.6312 / T
delta_eta_r = rho_Lemm_mol(p, T) / 10.4477

eta_r_Lemm = 0.000001 * (10.72 * (tau_eta_r ^ 0.2) * (delta_eta_r ^ 1) * Exp(-0 * delta_eta_r ^ 0) + 1.122 * (tau_eta_r ^ 0.05) * (delta_eta_r ^ 4) * Exp(-0 * delta_eta_r ^ 0) + 0.002019 * (tau_eta_r ^ 2.4) * (delta_eta_r ^ 9) * Exp(-0 * delta_eta_r ^ 0) + (-8.876) * (tau_eta_r ^ 0.6) * (delta_eta_r ^ 1) * Exp(-1 * delta_eta_r ^ 1) + (-0.02916) * (tau_eta_r ^ 3.6) * (delta_eta_r ^ 8) * Exp(-1 * delta_eta_r ^ 1))

End Function

Function eta_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: dynamische Viskosität in Pa*s

eta_trL = eta_0_Lemm(T) + eta_r_Lemm(p, T)

End Function

Function lambda_0_Lemm(T As Double) As Double
'Eingabe: T in K
'Ausgabe: Idealgasanteil der Wärmeleitfähigkeit in W/mK

tau_lambda_0 = 132.6312 / T

lambda_0_Lemm = 0.001 * (1.308 * (eta_0_Lemm(T) * 1000000) + 1.405 * (tau_lambda_0 ^ -1.1) + (-1.036) * (tau_lambda_0 ^ -0.3))

End Function

Function lambda_r_Lemm(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Residualanteil der Wärmeleitfähigkeit in W/mK

tau_lambda_r = 132.6312 / T
delta_lambda_r = rho_Lemm_mol(p, T) / 10.4477

lambda_r_Lemm = 0.001 * (8.743 * (tau_lambda_r ^ 0.1) * (delta_lambda_r ^ 1) * Exp(-0 * delta_lambda_r ^ 0) + 14.76 * (tau_lambda_r ^ 0#) * (delta_lambda_r ^ 2) * Exp(-0 * delta_lambda_r ^ 0) + (-16.62) * (tau_lambda_r ^ 0.5) * (delta_lambda_r ^ 3) * Exp(-1 * delta_lambda_r ^ 2) + 3.793 * (tau_lambda_r ^ 2.7) * (delta_lambda_r ^ 7) * Exp(-1 * delta_lambda_r ^ 2) + (-6.142) * (tau_lambda_r ^ 0.3) * (delta_lambda_r ^ 7) * Exp(-1 * delta_lambda_r ^ 2) + (-0.3778) * (tau_lambda_r ^ 1.3) * (delta_lambda_r ^ 11) * Exp(-1 * delta_lambda_r ^ 2))

End Function

Function lambda_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Wärmeleitfähigkeit in W/mK

lambda_trL = lambda_0_Lemm(T) + lambda_r_Lemm(p, T)

End Function

Function ny_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: kinematische Viskosität in m²/s

ny_trL = eta_trL(p, T) / rho_Lemm(p, T)

End Function

Function a_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Temperaturleitfähigkeit in m²/s

a_trL = lambda_trL(p, T) / cp_Lemm(p, T) / rho_Lemm(p, T)

End Function

Function Pr_trL(p As Double, T As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Prandtl-Zahl

Pr_trL = 1000 * ny_trL(p, T) / a_trL(p, T)

End Function


Function cp_VDI(T As Double) As Double
'Eingabe: T in K
'Ausgabe: spezifische isobare Wärmekapazität von Wasserdampf als Idealgas

VDI_T0 = 273.15
M_VDI = 18.01528

VDI_b1 = 0
VDI_b2 = -1.5
VDI_b3 = -1.25
VDI_b4 = -0.75
VDI_b5 = -0.5
VDI_b6 = -0.25
VDI_b7 = 0.25
VDI_b8 = 0.5
VDI_b9 = 0.75
VDI_b10 = 1

VDI_cp1 = -4747782.033
VDI_cp2 = 47999.19289
VDI_cp3 = -193158.8954
VDI_cp4 = 1460728.34
VDI_cp5 = -4000075.762
VDI_cp6 = 5576209.858
VDI_cp7 = 2570488.297
VDI_cp8 = -867052.1019
VDI_cp9 = 166642.939
VDI_cp10 = -13966.3462

VDI_tau = T / VDI_T0

cp_ZW = VDI_cp1 * VDI_tau ^ VDI_b1 + VDI_cp2 * VDI_tau ^ VDI_b2 + VDI_cp3 * VDI_tau ^ VDI_b3 + VDI_cp4 * VDI_tau ^ VDI_b4 + VDI_cp5 * VDI_tau ^ VDI_b5 + VDI_cp6 * VDI_tau ^ VDI_b6 + VDI_cp7 * VDI_tau ^ VDI_b7 + VDI_cp8 * VDI_tau ^ VDI_b8 + VDI_cp9 * VDI_tau ^ VDI_b9 + VDI_cp10 * VDI_tau ^ VDI_b10

cp_VDI = cp_ZW / M_VDI

End Function

Function h_VDI(T As Double) As Double
'Eingabe: T in K
'Ausgabe: spezifische Enthalpie von Wasserdampf als Idealgas

VDI_T0 = 273.15
M_VDI = 18.01528

VDI_b1 = 0
VDI_b2 = -1.5
VDI_b3 = -1.25
VDI_b4 = -0.75
VDI_b5 = -0.5
VDI_b6 = -0.25
VDI_b7 = 0.25
VDI_b8 = 0.5
VDI_b9 = 0.75
VDI_b10 = 1

VDI_h0 = -757488856.3
VDI_h1 = -1296856662
VDI_h2 = -26221959.08
VDI_h3 = 211045409.1
VDI_h4 = 1595991784
VDI_h5 = -2185241389#
VDI_h6 = 2030855630
VDI_h7 = 561703102.7
VDI_h8 = -157890187.8
VDI_h9 = 26010582.16
VDI_h10 = -1907453.732

VDI_tau = T / VDI_T0

h_ZW = VDI_h0 + VDI_h1 * VDI_tau ^ (VDI_b1 + 1) + VDI_h2 * VDI_tau ^ (VDI_b2 + 1) + VDI_h3 * VDI_tau ^ (VDI_b3 + 1) + VDI_h4 * VDI_tau ^ (VDI_b4 + 1) + VDI_h5 * VDI_tau ^ (VDI_b5 + 1) + VDI_h6 * VDI_tau ^ (VDI_b6 + 1) + VDI_h7 * VDI_tau ^ (VDI_b7 + 1) + VDI_h8 * VDI_tau ^ (VDI_b8 + 1) + VDI_h9 * VDI_tau ^ (VDI_b9 + 1) + VDI_h10 * VDI_tau ^ (VDI_b10 + 1)

h_VDI = h_ZW / M_VDI

End Function

Function s_VDI(p As Double, T As Double) As Double
'Eingabe: T in K
'Ausgabe: spezifische Enthalpie von Wasserdampf als Idealgas

VDI_T0 = 273.15
VDI_p0 = 101.325
M_VDI = 18.01528
RM_VDI = 8.314472

VDI_b1 = 0
VDI_b2 = -1.5
VDI_b3 = -1.25
VDI_b4 = -0.75
VDI_b5 = -0.5
VDI_b6 = -0.25
VDI_b7 = 0.25
VDI_b8 = 0.5
VDI_b9 = 0.75
VDI_b10 = 1

VDI_s0 = 7373724.814
VDI_s1 = -4747782.033
VDI_s2 = -31999.46193
VDI_s3 = 154527.1163
VDI_s4 = -1947637.787
VDI_s5 = 8000151.524
VDI_s6 = -22304839.43
VDI_s7 = 10281953.19
VDI_s8 = -1734104.204
VDI_s9 = 222190.5853
VDI_s10 = -13966.3462

VDI_tau = T / VDI_T0

s_ZW = VDI_s0 + VDI_s1 * Log(VDI_tau) - RM_VDI * Log(p / VDI_p0) + VDI_s2 * VDI_tau ^ VDI_b2 + VDI_s3 * VDI_tau ^ VDI_b3 + VDI_s4 * VDI_tau ^ VDI_b4 + VDI_s5 * VDI_tau ^ VDI_b5 + VDI_s6 * VDI_tau ^ VDI_b6 + VDI_s7 * VDI_tau ^ VDI_b7 + VDI_s8 * VDI_tau ^ VDI_b8 + VDI_s9 * VDI_tau ^ VDI_b9 + VDI_s10 * VDI_tau ^ VDI_b10

s_VDI = s_ZW / M_VDI

End Function

Function rho_trL(p As Double, T As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K
'Ausgabe: Dichte trockener Luft in kg/m^3

rho_trL = p / 0.287117 / T / Z_trL(p, T) 'Lemmon-Werte (Realgas)

End Function

Function v_trL(p As Double, T As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K
'Ausgabe: spez. Volumen trockener Luft in kg/m^3

v_trL = 0.287117 * T * Z_trL(p, T) / p

End Function

Function ps(T As Double) As Double
'Eingabe: Temperatur T in K
'Ausgabe: Sättigungsdampfdruck bzw. Sublimationsdruck in kPa

If T < 273.16 Then

    ps = psubl(T) 'Sublimationsdampfdruck nach IAPWS-06
    
Else: ps = ps_I(T) 'Siededampfdruck nach IAPWS IF-97

End If

End Function

Function xs(p As Double, T As Double)
'Eingabe: Druck p in kPa, Temperatur T in K
'Ausgabe: Sättigungwassergehalt xs in kg_W/kg_trL

Mo_L = 28.9586
Mo_W = 18.015257

If p - ps(T) > 0 Then

    xs = (Mo_W / Mo_L) * ps(T) / (p - ps(T))
    
Else: xs = -1 'Fehlermeldung bei unmöglicher Wasserbeladung

End If

End Function

Function yw(xw As Double) As Double
'Eingabe: Wassergehalt xw in kg_W/kg_trL
'Ausgabe: molarer Wassergehalt yw in mol_W/mol_trL

Mo_L = 28.9586
Mo_W = 18.015257

yw = (Mo_L / Mo_W) * xw

End Function

Function ys(p As Double, T As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K
'Ausgabe: molarer Sättigungswassergehalt ys in mol_w/mol_trL

ys = yw(xs(p, T))

End Function

Function pW(p As Double, T As Double, xw As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K, Wassergehalt in kg_W/kg_trL
'Ausgabe: Partialdruck Wasserdampf in kPa

If p - ps(T) < 0 Then 'Fehlermeldung bei unmöglicher Wasserbeladung
    
    pW = -1

Else:

    If xs(p, T) - xw >= 0 Then 'ungesättigter und gesättigter Zustand

        pW = ps(T) * phi_W(p, T, xw)

    ElseIf xs(p, T) - xw < 0 Then pW = ps(T) 'übersättigter Zustand

    End If
    
End If

End Function

Function rho_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K, xw in kg_W/kg_trL
'Ausgabe: Dichte feuchter Luft in kg/m^3

rho_fL = 1 / v_fL(p, T, xw)

End Function

Function v_fL(p As Double, T As Double, xw As Double)
'Eingabe: Druck p in kPa, Temperatur T in K, xw in kg_W/kg_trL
'Ausgabe: spez. Volumen feuchter Luft in kg/m^3

If pW(p, T, xw) + 1 = 0 Then 'Fehlermeldung bei unmöglicher Wasserbeladung

    v_fL = -1

Else:

    If xw <= xs(p, T) Then 'Luft bis Sättigungszustand

        v_fL = v_trL(p, T) * (1 + yw(xw))

    ElseIf xw > xs(p, T) Then 'übersättigte Luft
    
        If T >= 273.16 Then v_K = v_W_r1(p, T)

        If T < 273.16 Then v_K = v_Ice(p, T)

            v_fL = ((v_trL(p, T) * (1 + yw(xs(p, T)))) + v_K * (xw - xs(p, T)))
    
    End If

End If

End Function

Function v_fL_p(p As Double, T As Double, phi As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K, xw in kg_W/kg_trL
'Ausgabe: spez. Volumen feuchter Luft in kg/m^3

If phi - 1 <= 0 Then

    v_fL_p = v_trL(p, T) * (1 + yw(xw_phi(p, T, phi)))

Else: v_fL_p = -1 'relative Luftfeuchte nur bis Sättigungszustand definiert

End If

End Function

Function phi_W(p As Double, T As Double, xw As Double) As Double
'Eingabe: Druck p in kPa, Temperatur T in K, xw in kg_W/kg_trL
'Ausgabe: rel. Luftfeuchte phi in %

Mo_L = 28.9586
Mo_W = 18.015257

If 0 <= xw <= xs(p, T) Then

    phi_W = p * xw / ps(T) / (xw + (Mo_W / Mo_L))

Else: phi_W = -1 'relative Luftfeuchte nur bis Sättigungszustand definiert

End If

End Function

Function xw_phi(p As Double, T As Double, phi As Double) As Double
'Eingabe: p in kPa, T in K, rel. Luftfeuchte phi
'Ausgabe: Wassergehalt in kg_W/kg_trL

Mo_L = 28.9586
Mo_W = 18.015257

If phi <= 1 Then 'relative Luftfeuchte nur bis Sättigungszustand definiert

    xw_phi = (Mo_W / Mo_L) * phi * ps(T) / (p - phi * ps(T))
    
Else: xw_phi = -1

End If

End Function

Function yw_phi(p As Double, T As Double, phi As Double) As Double
'Eingabe: p in kPa, T in K, rel. Luftfeuchte phi
'Ausgabe: Wassergehalt in kg_W/kg_trL

If phi <= 1 Then 'relative Luftfeuchte nur bis Sättigungszustand definiert

    yw_phi = phi * ps(T) / (p - phi * ps(T))
    
Else: yw_phi = -1

End Function

Function xi_trL(xw As Double) As Double
'Eingabe: Wassergehalt in kg_W/kg_trL
'Ausgabe: Massenanteil trockene Luft in kg_trL/kg_fL

xi_trL = 1 / (1 + xw)

End Function

Function xi_WD(p As Double, T As Double, xw As Double) As Double
'Eingabe: Wassergehalt in kg_W/kg_trL
'Ausgabe: Massenanteil Wasserdampf in kg_W/kg_fL

If xw - xs(p, T) < 0 Then

    xi_WD = xw / (1 + xw)
    
Else: xi_WD = xs(p, T) / (1 + xw) 'Sättigungszustand, für Bilanzschließung evtl. xi_WK erforderlich!

End If

End Function

Function xi_WK(p As Double, T As Double, xw As Double) As Double
'Eingabe: Wassergehalt in kg_W/kg_trL
'Ausgabe: Massenanteil flüssiges oder festes Kondensat in kg_W/kg_fL

If xw - xs(p, T) <= 0 Then

    xi_WK = 0

Else: xi_WK = (xw - xs(p, T)) / (1 + xw) 'existiert nur im übersättigten Zustand

End If

End Function

Function psi_trL(xw As Double) As Double
'Eingabe: Wassergehalt in kg_W/kg_trL
'Ausgabe: Stoffmengenanteil trockene Luft in mol_trL/mol_fL

If xw = 0 Then

    psi_trL = 1
    
Else: psi_trL = 1 / (1 + yw(xw))

End If

End Function

Function psi_WD(p As Double, T As Double, xw As Double) As Double
'Eingabe: Wassergehalt in kg_W/kg_trL
'Ausgabe: Stoffmengenanteil Wasserdampf in mol_W/mol_fL

If yw(xw) - ys(p, T) < 0 Then

    psi_WD = yw(xw) / (1 + yw(xw))

Else: psi_WD = ys(p, T) / (1 + yw(xw)) 'Sättigungszustand, für Bilanzschließung evtl. psi_WK erforderlich!

End If

End Function

Function psi_WK(p As Double, T As Double, xw As Double) As Double
'Eingabe: Wassergehalt in kg_W/kg_trL
'Ausgabe: Stoffmengenanteil flüssiges oder festes Kondensat in mol_W/mol_fL

If yw(xw) - ys(p, T) <= 0 Then

    psi_WK = 0
    
Else: psi_WK = (yw(xw) - ys(p, T)) / (1 + yw(xw)) 'existiert nur im übersättigten Zustand

End If

End Function


Function cp_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_L
'Ausgabe: spez. isob. Wärmekapazität feuchter Luft in kJ/kg/K

If xw <= xs(p, T) Then

    If T < 273.16 Then
    
        cp_fL = cp_trL(p - pW(p, T, xw), T) + (cp_VDI(T) - cp_trL(p - pW(p, T, xw), T)) * xi_WD(p, T, xw)

        'cp_fL = 1.01 + (1.86 - 1.01) * xi_WD(p, T, xw) 'Berechnung wie LibLUFT_Stud
    
        'cp_fL = cp_trL(p - pW(p, T, xw), T) * psi_trL(xw) + cp_W(pW(p, T, xw), T) * psi_WD(p, T, xw) 'nach PTB 09

    Else: cp_fL = cp_trL(p - pW(p, T, xw), T) + (cp_W(pW(p, T, xw), T) - cp_trL(p - pW(p, T, xw), T)) * xi_WD(p, T, xw)
    
    End If
    
Else:

    cp_fL = -1 'Berechnung nur für Luft bis Sättigungszustand möglich
    
End If

End Function

Function h_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_L
'Ausgabe: spez. Enthalpie feuchter Luft in kJ/kg

If pW(p, T, xw) + 1 = 0 Then 'Fehlermeldung bei unmöglicher Wasserbeladung

    h_fL = -1

Else:

    If xw <= xs(p, T) Then 'Luft bis Sättigungszustand

        h_fL = h_trL(p - pW(p, T, xw), T) + h_W(pW(p, T, xw), T) * xw

    ElseIf xw > xs(p, T) Then 'übersättigte Luft
    
        If T >= 273.16 Then h_K = h_W_r1(p, T)

        If T < 273.16 Then h_K = h_Ice(p, T)
    
            h_fL = h_trL(p - ps(T), T) + h_W(ps(T), T) * xs(p, T) + h_K * (xw - xs(p, T))

    End If
    
End If

End Function

Function u_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_L
'Ausgabe: spez. innere Energie feuchter Luft in kJ/kg

u_fL = h_fL(p, T, xw) - p * v_fL(p, T, xw)

End Function

Function s_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_L
'Ausgabe: spez. Entropie feuchter Luft in kJ/kg/K

If pW(p, T, xw) + 1 = 0 Then 'Fehlermeldung bei unmöglicher Wasserbeladung

    s_fL = -1

Else:

    If xw = 0 Then
    
        s_fL = s_trL(p, T)

    ElseIf xw <= xs(p, T) Then 'Luft bis Sättigungszustand

        s_fL = s_trL(p - pW(p, T, xw), T) + s_W(pW(p, T, xw), T) * xw

    ElseIf xw > xs(p, T) Then 'übersättigte Luft
    
        If T >= 273.16 Then s_K = s_W_r1(p, T)

        If T < 273.16 Then s_K = s_Ice(p, T)
    
            s_fL = s_trL(p - ps(T), T) + s_W(ps(T), T) * xs(p, T) + s_K * (xw - xs(p, T))

    End If
    
End If

End Function

Function eta_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_L
'Ausgabe: dynam. Visk. in Pa*s

y_WK = xi_WK(p, T, xw) * v_W_r1(p, T) / v_fL(p, T, xw) 'Volumenanteil der Kondensatphase
y_G = 1 - y_WK 'Volumenanteil der Gasphase

If pW(p, T, xw) + 1 = 0 Then 'Fehlermeldung bei unmöglicher Wasserbeladung

    eta_fL = -1
    
Else:
    
    If xw = 0 Then 'trockene Luft
    
        eta_fL = eta_trL(p, T)
    
    ElseIf xw <= xs(p, T) Then 'Luft bis Sättigungszustand
    
        eta_fL = eta_trL(p - pW(p, T, xw), T) * psi_trL(xw) + eta_W(pW(p, T, xw), T) * psi_WD(p, T, xw)
    
    ElseIf xw > xs(p, T) Then 'übersättigte feuchte Luft
    
        If T >= 273.15 Then 'Flüssignebel
            
            eta_fL = (eta_trL(p - ps(T), T) * psi_trL(xs(p, T)) + eta_W(ps(T), T) * psi_WD(p, T, xs(p, T))) * y_G + eta_W(p, T) * y_WK
    
        Else: eta_fL = eta_trL(p - ps(T), T) * psi_trL(xs(p, T)) + eta_W(ps(T), T) * psi_WD(p, T, xs(p, T)) 'Eisnebel, Eiskristalle werden vernachlässigt
        
        End If
    
    End If

End If

End Function

Function lambda_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_L
'Ausgabe: Wärmeleitfähigkeit in W/m/K

y_WK = xi_WK(p, T, xw) * v_W_r1(p, T) / v_fL(p, T, xw)
y_EK = xi_WK(p, T, xw) * v_Ice(p, T) / v_fL(p, T, xw)

If T < 273.16 Then

    y_K = y_EK
    
Else: y_K = y_WK

End If 'Volumenanteil der festen oder flüssigen Kondensatphase

y_G = 1 - y_K 'Volumenanteil der Gasphase

If pW(p, T, xw) + 1 = 0 Then 'Fehlermeldung bei unmöglicher Wasserbeladung

    lambda_fL = -1
    
Else:

    If xw = 0 Then 'trockene Luft
    
        lambda_fL = lambda_trL(p, T)
    
    ElseIf xw <= xs(p, T) Then 'Luft bis Sättigungszustand
    
        lambda_fL = lambda_trL(p - pW(p, T, xw), T) * psi_trL(xw) + lambda_W(pW(p, T, xw), T) * psi_WD(p, T, xw)
    
    ElseIf xw > xs(p, T) Then 'Eis- oder Flüssignebel
    
        lambda_fL = (lambda_trL(p - ps(T), T) * psi_trL(xs(p, T)) + lambda_W(ps(T), T) * psi_WD(p, T, xs(p, T))) * y_G + lambda_W(p, T) * y_K
        
    End If

End If

End Function

Function ny_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_L
'Ausgabe: kinem. Visk. in m²/s

ny_fL = eta_fL(p, T, xw) / rho_fL(p, T, xw)

End Function

Function a_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Temperaturleitfähigkeit in m²/s

a_fL = lambda_fL(p, T, xw) / cp_fL(p, T, xw) / rho_fL(p, T, xw)

End Function

Function Pr_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K
'Ausgabe: Prandtl-Zahl

Pr_fL = 1000 * ny_fL(p, T, xw) / a_fL(p, T, xw)

End Function

Function M_fL(p As Double, T As Double, xw As Double) As Double
'Eingabe: p in kPa, T in K, xw in kg_W/kg_L
'Ausgabe: scheinbare molare Masse feuchter Luft in kg/kmol

M_trL = 28.9586
M_W = 18.015268

M_fL = psi_trL(xw) * M_trL + (psi_WD(p, T, xw) + psi_WK(p, T, xw)) * M_W

End Function

Function T_phx(p As Double, h_0 As Double, xw As Double) As Double
'Eingabe: p in kPa, h_0 in kJ/kg_L, xw in kg_W/kg_L
'Ausgabe: Temperatur feuchter Luft in K

Dim T_o, T_u, T_p As Double

T_o = 353.15
T_u = 263.15
T_p = (T_o + T_u) / 2
eps = 0.00001

Do Until T_o - T_u < eps

    If h_fL(p, T_p, xw) - h_0 > 0 Then
        T_o = T_p
        T_p = (T_o + T_u) / 2
        
    ElseIf h_fL(p, T_p, xw) - h_0 < 0 Then
    
        T_u = T_p
        T_p = (T_o + T_u) / 2
        
    Else: T_o = T_u

End If

Loop

T_phx = Round(T_p, 5)

End Function

Function T_psx(p As Double, s_0 As Double, xw As Double) As Double
'Eingabe: p in kPa, s_0 in kJ/kg_L/K, xw in kg_W/kg_L
'Ausgabe: Temperatur feuchter Luft in K

Dim T_o, T_u, T_p As Double

T_o = 353.15
T_u = 263.15
T_p = (T_o + T_u) / 2
eps = 0.00001

Do Until T_o - T_u < eps

    If s_fL(p, T_p, xw) - s_0 > 0 Then
        T_o = T_p
        T_p = (T_o + T_u) / 2
        
    ElseIf s_fL(p, T_p, xw) - s_0 < 0 Then
    
        T_u = T_p
        T_p = (T_o + T_u) / 2
        
    Else: T_o = T_u

End If

Loop

T_psx = Round(T_p, 5)

End Function

Function x_pTv(p As Double, T As Double, v_0 As Double) As Double
'Eingabe: p in kPa, T in K, v_0 in m³/kg_L
'Ausgabe: Wassergehalt feuchter Luft in kg_W/kg_L

Dim x_o, x_u, x_p As Double

x_o = 1
x_u = 0
x_p = (x_o + x_u) / 2
eps = 0.00001

Do Until x_o - x_u < eps

    If v_fL(p, T, x_p) - v_0 > 0 Then
        x_o = x_p
        x_p = (x_o + x_u) / 2
        
    ElseIf v_fL(p, T, x_p) - v_0 < 0 Then
    
        x_u = x_p
        x_p = (x_o + x_u) / 2
        
    Else: x_o = x_u

End If

Loop

x_pTv = Round(x_p, 5)

End Function

Function tF(p, T, xw)
'Eingabe: T in K, xw in kg_W/kg_L, p in kPa
'Ausgabe: tF in °C

Dim pW As Double, del As Double, eps As Double, def1 As Double, def2 As Double, tF1 As Double, tF2 As Double, i As Integer

pU = p / 100
tL = T - 273.15

pW = pU * xw / (0.6215469 + xw)  'in bar
tF = tL - 1: eps = 0.01: del = 1# / eps: def1 = 0#: i = 1

Do Until Abs(del) < eps Or i > 1000: i = i + 1
     tF1 = tL + 755 / (0.5 * pU) * (pW - pds(tF)): def1 = tF - tF1
     tF2 = tL + 755 / (0.5 * pU) * (pW - pds(tF + 0.1)): def2 = tF - tF2
     del = def1 * eps / (def2 - def1): tF = tF - del
     
Loop

If i > 1000 Then tF = -1: MsgBox "ERROR with alternative function for tF: i > 1000!"

If tF > tL Then: tF = -2: MsgBox "ERROR with alternative function for tF: tF > tL!"

End If

End Function

