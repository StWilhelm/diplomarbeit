%************************************************************************
% Buchklasse Institut f?r Thermodynamik und TGA
%
% Sebastian Pinnau, 2008
%
%************************************************************************

\def\fileversion{0.2}
\def\filedate{2008/10/10}
\def\filename{thermobook}

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{\filename}[\filedate Buchklasse Institut f?r Thermodynamik und TGA]
\typeout{Class: '\filename' Version \fileversion,  Buchklasse Institut f?r Thermodynamik und TGA}

\RequirePackage{xkeyval}


%************************************************************************
% Klassenoptionen
%
%************************************************************************

\newif\if@thesisstyle
\newif\if@reportstyle
\DeclareOptionX{thesis}{\@thesisstyletrue}
\DeclareOptionX{report}{\@reportstyletrue}

\def\@color{white}
\DeclareOptionX{color}{\def\@color{#1}}

\ProcessOptionsX


%************************************************************************
% Dokumenteinstellungen / Laden von Paketen
%
%************************************************************************

\LoadClass[%
paper=a4,           % Papierformat
fontsize=12pt,      % Zeichensatzgroesse
twoside=false,      % einseitiger Druck (true fuer doppelseitigen Druck, Koma-Script)
headings=normal,     % Ueberschriftengroesse (alternativ big- oder smallheadings, Koma-Script)
fleqn,              % linksbuendige Ausrichtung von Gleichungen (sonst zentriert, Koma-Script)
parskip=false,      % Absatzkennzeichnung durch Einzug (alternativ Zeilenabstand, Koma-Script)
numbers=noenddot,   % letzte Ziffer der Nummerierung ohne nachfolgenden Punkt (Koma-Script)
captions=tableheading,  % caption von Tabellen als Tabellenueberschrift formatieren (Koma-Script)
BCOR10mm,           % Bindekorrektur (erweiterter Rand)
DIV=12              % automatische Satzspiegelberechnung (Koma-Script)
]
{scrbook}           % Dokumentklasse (alternativ scrartcl oder scrreprt, Koma-Script) 


% Paket zur manuellen Einstellung der R�nder
\usepackage{geometry}

% Paket zur Gestaltung der Kolumnentitel
\usepackage[headsepline]{scrpage2}

% Flattersatz (unterer Seiten-Rand), bei beidseitigem Druck empfiehlt sich aus
% optischen Gruenden \flushbottom (Zeilen enden immer auf der gleichen Hoehe)
\raggedbottom

% deutsche Trennungsregeln, deutsche Zusatzbefehle
\RequirePackage[ngerman]{babel}

% deutsches Fontencoding, T1 Schriften
\RequirePackage[T1]{fontenc}

% Schriftfamilie ComputerModern Bright (serifenlos)
\RequirePackage{cmbright}


% AMS-LaTeX Pakete f�r mathematischen Formelsatz
\RequirePackage{amsmath,amssymb,amsthm}

% skalierte Fonts f�r mathematischen Formelsatz
\RequirePackage{exscale}

% optischer Randausgleich (verschiedene Buchstaben, Satzzeichen, Bindestriche
% etc. werden leicht in den Seitenrand verschoben); automatische Skalierung von
% Buchstaben zur Verbesserung des Blocksatzes
\RequirePackage{microtype}

%f?r Vekotrschriften innerhalb der $-Umgebung
%\RequirePackage{lmodern} 

% stellt diverse Sonderzeichen zur Verfuegung z.B. \texteuro
\RequirePackage{textcomp}

% Silbentrennung wird auch bei linksbuendigem Text angewendet,
% z.B. im Index
\RequirePackage{ragged2e}

% das setspace package bietet die Moeglichkeit zur Aenderung des Durchschusses
% (Zeilenabstand im Blocksatz)
\RequirePackage{setspace}

% Einrueckung der Formeln (sinnvoll in Verbindung mit linksbuendigen Formeln)
\mathindent0.9cm

% zusaetzliche Hinweise zur Seite auf der sich ein Verweis befindet
% z.B. mit dem Befehl \vref{label}
\RequirePackage[german]{varioref}

% laedt die Bibliographie-Erweiterung natbib, ermoeglicht die Verwendung des
% Bibliographie-Styles natdin (DIN 1505)
% [numbers] Verwendung von numerischen Einordnungsmarken
% [sort] alphabetische Sortierung des Literaturverzeichnisses
\RequirePackage[numbers, sort]{natbib}

% setzt den Bibliographie Style natdin nach DIN 1505
\bibliographystyle{natdin} %Sortierung nach Nachname Autor
%\bibliographystyle{unsrtdin} %Sortierung nach Vorkommen im Text

% Farbunterstuetzung fuer dvi-Dokumente
\RequirePackage[dvips]{color}

% Grafikunterstuetzung f�r LaTeX (Einfuegen von Bildern)
\RequirePackage{graphicx}

% Anordnung mehrerer Grafiken
\RequirePackage{subfig}

% Paket zum Drehen von Text, Tabellen, Grafiken etc.; das Paket stellt u.A.
% die sidewaystable- und sidewaysfigure-Umgebungen zur Verfuegung, mit denen
% Fliessobjekte im Querformat (auf einer eigenen Seite) platziert werden koennen
\RequirePackage{rotating}

% float-Objekte werden nur innerhalb der aktuellen section verschoben, ohne
% dieses Paket koennen floats auch in die naechste section uebernommen werden
\RequirePackage{float}
\RequirePackage[section]{placeins}

% laedt das Grafikpaket tikz sowie einige Bibliotheken dazu
\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes}

% stellt Befehle zum Setzen von Einheiten zur Verfuegung
\RequirePackage[amssymb,pstricks]{SIunits}

% Paket zum Setzen von Einheiten und zur Formatierung von Zahlen
% Einheiten werden immer "gerade" gesetzt, Zahlformatierung mit Tausender-
% trennzeichen, Dezimalkommas ohne nachfolgenden Abstand
% Befehle: \SI{Zahl}{Einheit}, \num{Zahl}
\RequirePackage{sistyle}
\SIstyle{German}

% longtable-Umgebung ein (Tabellen mit moeglichem Seitenumbruch)
\RequirePackage{longtable}

% erweiterte Tabellenbefehle (zur Erstellung wirklich schoener Tabellen
% fuer den Buchdruck)
\RequirePackage{booktabs}

% erweiterte Moeglichkeiten zur Schriftformatierung in Tabellenspalten
\RequirePackage{array}

% Paket zum Zusammenfuegen von Tabellenzeilen
\RequirePackage{multirow}

% Paket zur Indexerstellung
\RequirePackage{makeidx}

% Erstellung des Index, Indexeintraege erfolgen mit \index{Name des Eintrages}
\makeindex

% erstellt Anweisungen, die nur fuer pdftex gelten
\RequirePackage{ifpdf}

% implementiert if Schleifen
\RequirePackage{ifthen}


% Paket zur automatischen Linkerstellung f�r Inhaltsverzeichnis, Literaturverweise etc.
% [plainpages=false] setzt getrennte Links fuer gleiche Seitenzahlen (z.B. fuer i und 1)
% [pdfpagelabels] geaendertes Anzeigeformat der Seitennummern im AcrobatReader (roemische
% Seitennummern werden angezeigt)
\RequirePackage[plainpages=false, pdfpagelabels]{hyperref}


% setzt Strafpunkte f�r Schusterjungen und Hurenkinder hoch (dies kann sich eventuell
% nachteilig bei  Verwendung von \flushbottom auswirken)
\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

% erneute Berechnung des Satzspiegels [Bindekorrektur]{Satzspiegel}
% (notwendig falls nach der Festlegung der Dokumentenklasse die Font-
% familie gewechselt wird), die Angabe [current] sorgt dafuer, dass die
% oben bereits gewaehlte Bindekorrektur uebernommen wird
% {last} verwendet den oben bereits vorgegebenen festen DIV-Wert,
% alternativ kann mit {calc} ein DIV-Wert berechnet werden
\typearea[current]{last}

%Kopfzeile wird NICHT kursiv
\renewcommand*{\headfont}{\normalfont}

\pdfminorversion=5
\pdfobjcompresslevel=3
\pdfcompresslevel=9


%Erm?glicht das Einbinden von PDF-Seiten
\usepackage{pdfpages}

%Einbinden von Quelltexten
\usepackage{listings} 

%F?r das Durchstreichen von W?rtern
\usepackage{cancel}

%macht Abbildung und Tabllenverzeichnis ins Inhaltsverzeichnis
\usepackage[nottoc]{tocbibind}

%Definiert eine neue Spaltenart f?r die Arrays
\newcolumntype{C}{ >{\centering\arraybackslash} m{0.5cm} }

%Definiert eine neue Spaltenart f?r die Arrays
\newcolumntype{S}{ >{\centering\arraybackslash} m{0.35cm} }

%Definiert eine neue Spaltenart f?r die Arrays
\newcolumntype{J}{ >{\centering\arraybackslash} m{0.6cm} }

%Definiert eine neue Spaltenart f?r die Arrays
\newcolumntype{L}{ >{\centering\arraybackslash} m{0.2cm} }

%************************************************************************
% Anpassungen des Dokumentenlayouts
%
%************************************************************************

%Anpassung der Kolumnentitel, Seitennummerierung oben, Schriftgr��en
\clearscrheadfoot
\ihead{\headmark}
\ohead{\pagemark}
\pagestyle{scrheadings}
\addtokomafont{pageheadfoot}{\footnotesize}
\addtokomafont{pagenumber}{\normalsize}

% kleinere Schriftgr��e f�r Bildunterschriften/Tabellen�berschriften
\addtokomafont{caption}{\footnotesize}

% demibold als Fettschrift verwenden
\renewcommand{\textbf}[1]{{\fontseries{sb}\selectfont#1}}   
\newcommand{\textbx}[1]{{\fontseries{b}\selectfont#1}}     
% Ueberschriften in LatinModernSans-demicondensed
\newcommand*{\headFont}{\fontfamily{lmss}\fontseries{sbc}\selectfont}
%\newcommand*{\headFont}{\fontfamily{cmbr}\fontseries{b}\selectfont}
\addtokomafont{disposition}{\headFont}

% \mathsf f�r cmbright richtig definieren
% (sonst wird bei \mathsf CM-SansSerif verwendet)
\renewcommand{\mathsf}[1]{\mathrm{#1}}     

% Farbschema wei�
\ifthenelse{\equal{\@color}{white}}{% 
}

% Farbschema blau
\ifthenelse{\equal{\@color}{blue}}{% 
  % Kapitelueberschriften blau
  \addtokomafont{chapter}{\color{HKS41-100}}
}

% Farbschema blau/wei�
\ifthenelse{\equal{\@color}{bluewhite}}{% 
  % Kapitelueberschriften blau
  \addtokomafont{chapter}{\color{HKS41-100}}
}




\usepackage[T1]{fontenc}
% Kapit?lchen
%\renewcommand{\textsc}[1]{\uppercase{#1}}
\usepackage{relsize}
\DeclareRobustCommand\fakesc[1]{%
  \begingroup%
  \xdef\fake@name{\csname\curr@fontshape/\f@size\endcsname}%
  \fontsize{\fontdimen9\fake@name}{\baselineskip}\selectfont%
  \uppercase{#1}%
  \endgroup%
} 

%************************************************************************
% Farbdefinitionen
%
%************************************************************************

% TU-Hausfarben (aus der LaTeX-Vorlage der TUD entnommen)
\definecolor{HKS41-100}{cmyk}{1.0, 0.7, 0.1, 0.5}
\definecolor{HKS41-90}{cmyk}{0.9, 0.63, 0.09, 0.45}
\definecolor{HKS41-80}{cmyk}{0.8, 0.56, 0.08, 0.4}
\definecolor{HKS41-70}{cmyk}{0.7, 0.49, 0.07, 0.35}
\definecolor{HKS41-60}{cmyk}{0.6, 0.42, 0.06, 0.3}
\definecolor{HKS41-50}{cmyk}{0.5, 0.35, 0.05, 0.25}
\definecolor{HKS41-40}{cmyk}{0.4, 0.28, 0.04, 0.2}
\definecolor{HKS41-30}{cmyk}{0.3, 0.21, 0.03, 0.15}
\definecolor{HKS41-20}{cmyk}{0.2, 0.14, 0.02, 0.1}
\definecolor{HKS41-10}{cmyk}{0.1, 0.07, 0.01, 0.05}
\definecolor{HKS44-100}{cmyk}{1.0, 0.5, 0.0, 0.0}
\definecolor{HKS44-90}{cmyk}{0.9, 0.45, 0.0, 0.0}
\definecolor{HKS44-80}{cmyk}{0.8, 0.4, 0.0, 0.0}
\definecolor{HKS44-70}{cmyk}{0.7, 0.35, 0.0, 0.0}
\definecolor{HKS44-60}{cmyk}{0.6, 0.3, 0.0, 0.0}
\definecolor{HKS44-50}{cmyk}{0.5, 0.25, 0.0, 0.0}
\definecolor{HKS44-40}{cmyk}{0.4, 0.2, 0.0, 0.0}
\definecolor{HKS44-30}{cmyk}{0.3, 0.15, 0.0, 0.0}
\definecolor{HKS44-20}{cmyk}{0.2, 0.1, 0.0, 0.0}
\definecolor{HKS44-10}{cmyk}{0.1, 0.05, 0.0, 0.0}
\definecolor{HKS36-100}{cmyk}{0.8, 0.9, 0.0, 0.0}
\definecolor{HKS36-90}{cmyk}{0.72, 0.81, 0.0, 0.0}
\definecolor{HKS36-80}{cmyk}{0.64, 0.72, 0.0, 0.0}
\definecolor{HKS36-70}{cmyk}{0.56, 0.63, 0.0, 0.0}
\definecolor{HKS36-60}{cmyk}{0.48, 0.54, 0.0, 0.0}
\definecolor{HKS36-50}{cmyk}{0.4, 0.45, 0.0, 0.0}
\definecolor{HKS36-40}{cmyk}{0.32, 0.36, 0.0, 0.0}
\definecolor{HKS36-30}{cmyk}{0.24, 0.27, 0.0, 0.0}
\definecolor{HKS36-20}{cmyk}{0.16, 0.18, 0.0, 0.0}
\definecolor{HKS36-10}{cmyk}{0.08, 0.09, 0.0, 0.0}
\definecolor{HKS33-100}{cmyk}{0.5, 1.0, 0.0, 0.0}
\definecolor{HKS33-90}{cmyk}{0.45, 0.9, 0.0, 0.0}
\definecolor{HKS33-80}{cmyk}{0.4, 0.8, 0.0, 0.0}
\definecolor{HKS33-70}{cmyk}{0.35, 0.7, 0.0, 0.0}
\definecolor{HKS33-60}{cmyk}{0.3, 0.6, 0.0, 0.0}
\definecolor{HKS33-50}{cmyk}{0.25, 0.5, 0.0, 0.0}
\definecolor{HKS33-40}{cmyk}{0.2, 0.4, 0.0, 0.0}
\definecolor{HKS33-30}{cmyk}{0.15, 0.3, 0.0, 0.0}
\definecolor{HKS33-20}{cmyk}{0.1, 0.2, 0.0, 0.0}
\definecolor{HKS33-10}{cmyk}{0.05, 0.1, 0.0, 0.0}
\definecolor{HKS57-100}{cmyk}{1.0, 0.0, 0.9, 0.2}
\definecolor{HKS57-90}{cmyk}{0.9, 0.0, 0.81, 0.18}
\definecolor{HKS57-80}{cmyk}{0.8, 0.0, 0.72, 0.16}
\definecolor{HKS57-70}{cmyk}{0.7, 0.0, 0.63, 0.14}
\definecolor{HKS57-60}{cmyk}{0.6, 0.0, 0.54, 0.12}
\definecolor{HKS57-50}{cmyk}{0.5, 0.0, 0.45, 0.1}
\definecolor{HKS57-40}{cmyk}{0.4, 0.0, 0.36, 0.08}
\definecolor{HKS57-30}{cmyk}{0.3, 0.0, 0.27, 0.06}
\definecolor{HKS57-20}{cmyk}{0.2, 0.0, 0.18, 0.04}
\definecolor{HKS57-10}{cmyk}{0.1, 0.0, 0.09, 0.02}
\definecolor{HKS65-100}{cmyk}{0.65, 0.0, 1.0, 0.0}
\definecolor{HKS65-90}{cmyk}{0.585, 0.0, 0.9, 0.0}
\definecolor{HKS65-80}{cmyk}{0.52, 0.0, 0.8, 0.0}
\definecolor{HKS65-70}{cmyk}{0.455, 0.0, 0.7, 0.0}
\definecolor{HKS65-60}{cmyk}{0.39, 0.0, 0.6, 0.0}
\definecolor{HKS65-50}{cmyk}{0.325, 0.0, 0.5, 0.0}
\definecolor{HKS65-40}{cmyk}{0.26, 0.0, 0.4, 0.0}
\definecolor{HKS65-30}{cmyk}{0.195, 0.0, 0.3, 0.0}
\definecolor{HKS65-20}{cmyk}{0.13, 0.0, 0.2, 0.0}
\definecolor{HKS65-10}{cmyk}{0.065, 0.0, 0.1, 0.0}
\definecolor{HKS07-100}{cmyk}{0.0, 0.6, 1.0, 0.0}
\definecolor{HKS07-90}{cmyk}{0.0, 0.54, 0.9, 0.0}
\definecolor{HKS07-80}{cmyk}{0.0, 0.48, 0.8, 0.0}
\definecolor{HKS07-70}{cmyk}{0.0, 0.42, 0.7, 0.0}
\definecolor{HKS07-60}{cmyk}{0.0, 0.36, 0.6, 0.0}
\definecolor{HKS07-50}{cmyk}{0.0, 0.3, 0.5, 0.0}
\definecolor{HKS07-40}{cmyk}{0.0, 0.24, 0.4, 0.0}
\definecolor{HKS07-30}{cmyk}{0.0, 0.18, 0.3, 0.0}
\definecolor{HKS07-20}{cmyk}{0.0, 0.12, 0.2, 0.0}
\definecolor{HKS07-10}{cmyk}{0.0, 0.06, 0.1, 0.0}



%************************************************************************
% Titelseite
%
%************************************************************************

% Variablen f�r Benutzereinstellungen
\def\@faculty{Maschinenwesen}
\def\@institute{Verfahrenstechnik und Umwelttechnik}
\def\@subTitle{}
\def\@thesis{Diplomarbeit}
\def\@studentID{3856139}
\def\@tutor{}
\def\@footLeft{}
\def\@footMiddle{}
\def\@footRight{}

\newcommand*{\faculty}[1]{\def\@faculty{#1}}
\newcommand*{\institute}[1]{\def\@institute{#1}}
\newcommand*{\subTitle}[1]{\def\@subTitle{#1}}
\newcommand*{\thesis}[1]{\def\@thesis{#1}}
\newcommand*{\studentID}[1]{\def\@studentID{#1}}
\newcommand*{\tutor}[1]{\def\@tutor{#1}}
\newcommand*{\titleFoot}[3]{\def\@footLeft{#1}\def\@footMiddle{#2}\def\@footRight{#3}}

\renewcommand{\maketitle}{%
  \begin{titlepage}    
        \begin{minipage}[t][220mm][l]{150mm}
          % <-- Diplom,Dissertation etc. -->
          \if@thesisstyle%
           \begin{center}
						\vfill
            \LARGE{T\fakesc{echnische} U\fakesc{niversi�t} D\fakesc{resden}}
            \vfill
            \vskip 1cm
            \large{Fakult�t Maschinenbau, Institut f�r Verfahrenstechnik und Umwelttechnik}
            \vfill
						\vskip 1.5cm
						%\normalsize{Von der}\\
						\normalsize{Der}\\
						\large{Professur Energieverfahrenstechnik}\\
						%\normalsize{angenommene}\\
						\normalsize{vorgelegte}\\
            \vskip 2cm
            {\LARGE\textbf\@thesis}
            \vskip 1.5cm
						\onehalfspacing
						{\Large\@title} 
            \vskip 2.5cm
            %\{\normalsize zur Erlangung des akademischen Grades}\\
            %\{\normalsize Diplom-Ingeniuer (Dipl.-Ing.)}\\
            \vfill
            \normalsize{von}\\
            \vskip 1cm
            {\large \@author} \\
						{\normalsize geb. am 19.11.1994 in Chemnitz}
            \vskip 0.2cm
            \vfill
            \vfill
            \vfill
            \vskip 2.5cm
            \vfill
            \vfill
           \end{center}
            {\normalsize
            \hspace*{15mm} betreuender Hochschullehrer:  \hspace*{6mm}   		Prof. Dr.-Ing. Michael Beckmann\\
            \hspace*{15mm} kooperierende Firma:						 \hspace*{20.3mm}   E\&S Planbau K�hlturmbau GmbH}
						%\hspace*{15mm} 						 \hspace*{26.5mm}   Prof. Dr.-Ing. habil. Hans-Joachim Kretzschmar}
            \vskip 0.5em
            {\large\@tutor}
            \vskip 1.3cm
            \begin{center}
            %{\normalsize Eingereicht im Dezember \@date}   
						\normalsize{Tag der Einreichung 04.12.2017}\\
            \end{center}       
            \vfill
            \phantom{x}
          \fi
          % <-- Bericht -->
          \if@reportstyle
            \vfill
            \vfill
            \vfill
            {\large \@author}
            \vskip 2em
            {\headFont\Huge\raggedright\@title}
            \vskip 1em
            {\headFont\Large\raggedright\@subTitle}
            \vskip 3em
            {\large\@date}       
            \vfill
            \phantom{x}
          \fi
        \end{minipage} 
\end{titlepage}
}


