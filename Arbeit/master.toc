\select@language {ngerman}
\contentsline {chapter}{\nonumberline Symbolverzeichnis}{IV}{chapter*.2}
\contentsline {chapter}{Abbildungsverzeichnis}{VI}{chapter*.6}
\contentsline {chapter}{Tabellenverzeichnis}{VII}{chapter*.7}
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Aufgabenstellung}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Zielstellung}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}Theoretische Grundlagen von W\"arme\"ubertragern}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Bauformen von W\"arme\"ubertragern: Einordnung des Trockenk\"uhlers}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Einteilung nach der thermischen Auslegung}{3}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Einteilung nach der Stofftrennung}{4}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Einteilung nach der Stromf\"uhrung}{4}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Einordnung des Trockenk\"uhlers}{4}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}adiabate Fahrweise des Trockenk\"uhlers}{5}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Generelle Annahmen und Bezeichnungen f\"ur den Trockenk\"uhler}{6}{subsection.2.1.6}
\contentsline {subsection}{\numberline {2.1.7}Temperaturverlauf im Trockenk\"uhler}{6}{subsection.2.1.7}
\contentsline {section}{\numberline {2.2}Literaturrecherche}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Methode A: Ausgleichstemperatur}{10}{subsection.2.2.1}
\contentsline {subsubsection}{\nonumberline Herleitung der Differentialgleichung}{11}{section*.10}
\contentsline {subsubsection}{\nonumberline Allgemeiner Fall}{12}{section*.12}
\contentsline {subsubsection}{\nonumberline Sonderfall: gleichgro\IeC {\ss }e Kapazit\"atsstr\"ome im Gegenstrom}{15}{section*.15}
\contentsline {subsection}{\numberline {2.2.2}Methode B: Betriebscharakteristik}{17}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Methode C: Mittlere logarithmische Temperaturdifferenz}{20}{subsection.2.2.3}
\contentsline {subsubsection}{\nonumberline Herleitung}{20}{section*.18}
\contentsline {subsubsection}{\nonumberline Umsetzung f\"ur die Berechnungsf\"alle}{22}{section*.19}
\contentsline {subsection}{\numberline {2.2.4}Vergleich der Berechnungsmethoden}{25}{subsection.2.2.4}
\contentsline {subsubsection}{\nonumberline Umformung der Berechnungsmethoden ineinander}{25}{section*.21}
\contentsline {paragraph}{\nonumberline Ausgleichstemperatur zu Betriebscharakteristik}{26}{section*.22}
\contentsline {paragraph}{\nonumberline Betriebscharakteristik zur mittleren logarithmischen Temperaturdifferenz}{26}{section*.23}
\contentsline {section}{\numberline {2.3}Plausibilit\"atskontrollen}{27}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Energiebilanz}{28}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Umsetzung in Berechnungsprogramme}{30}{chapter.3}
\contentsline {section}{\numberline {3.1}Anforderungen und Form der Berechnungsprogramme}{30}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Auslegungsprogramm f\"ur E\&S Planbau}{31}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Modul W\"arme\"ubertrager f\"ur ProcessExcel}{32}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Umsetzung der Berechnungsmethoden}{33}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Nachrechnungsfall}{34}{subsection.3.2.1}
\contentsline {subsubsection}{\nonumberline Methode A: Ausgleichstemperatur}{34}{section*.27}
\contentsline {subsubsection}{\nonumberline Methode B: Betriebscharakteristik}{34}{section*.28}
\contentsline {subsection}{\numberline {3.2.2}Auslegungsf\"alle}{36}{subsection.3.2.2}
\contentsline {subsubsection}{\nonumberline Methode A: Ausgleichstemperatur}{37}{section*.30}
\contentsline {subsubsection}{\nonumberline Methode B: Betriebscharakteristik}{37}{section*.31}
\contentsline {chapter}{\numberline {4}Ergebnisse}{39}{chapter.4}
\contentsline {section}{\numberline {4.1}Vergleich der Ergebnisse untereinander}{39}{section.4.1}
\contentsline {section}{\numberline {4.2}Einfluss der temperaturabh\"angigen W\"armekapazit\"aten}{44}{section.4.2}
\contentsline {chapter}{\numberline {5}Zusammenfassung und Ausblick}{47}{chapter.5}
\contentsline {section}{\numberline {5.1}Fazit}{47}{section.5.1}
\contentsline {section}{\numberline {5.2}Ausblick}{48}{section.5.2}
\contentsline {chapter}{Literaturverzeichnis}{49}{chapter*.42}
\contentsline {chapter}{\nonumberline Selbstst\"andigkeitserkl\"arung}{51}{chapter*.43}
